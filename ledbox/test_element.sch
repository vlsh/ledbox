EESchema Schematic File Version 4
LIBS:ledbox-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5E637029
P 3000 3500
AR Path="/5E63399D/5E637029" Ref="#PWR?"  Part="1" 
AR Path="/5E634176/5E637029" Ref="#PWR02"  Part="1" 
AR Path="/5E6426EF/5E637029" Ref="#PWR?"  Part="1" 
F 0 "#PWR02" H 3000 3250 50  0001 C CNN
F 1 "GND" H 3005 3327 50  0000 C CNN
F 2 "" H 3000 3500 50  0001 C CNN
F 3 "" H 3000 3500 50  0001 C CNN
	1    3000 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5E63777A
P 4050 3200
AR Path="/5E63399D/5E63777A" Ref="D?"  Part="1" 
AR Path="/5E634176/5E63777A" Ref="D2"  Part="1" 
AR Path="/5E6426EF/5E63777A" Ref="D?"  Part="1" 
F 0 "D2" H 4043 3416 50  0000 C CNN
F 1 "LED" H 4043 3325 50  0000 C CNN
F 2 "" H 4050 3200 50  0001 C CNN
F 3 "~" H 4050 3200 50  0001 C CNN
	1    4050 3200
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E638F7D
P 3450 3200
AR Path="/5E63399D/5E638F7D" Ref="R?"  Part="1" 
AR Path="/5E634176/5E638F7D" Ref="R2"  Part="1" 
AR Path="/5E6426EF/5E638F7D" Ref="R?"  Part="1" 
F 0 "R2" V 3245 3200 50  0000 C CNN
F 1 "R_Small_US" V 3336 3200 50  0000 C CNN
F 2 "" H 3450 3200 50  0001 C CNN
F 3 "~" H 3450 3200 50  0001 C CNN
	1    3450 3200
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5E63C2AF
P 4600 3100
AR Path="/5E63399D/5E63C2AF" Ref="H?"  Part="1" 
AR Path="/5E634176/5E63C2AF" Ref="H3"  Part="1" 
AR Path="/5E6426EF/5E63C2AF" Ref="H?"  Part="1" 
F 0 "H3" H 4700 3149 50  0000 L CNN
F 1 "Solder_Pad" H 4700 3058 50  0000 L CNN
F 2 "" H 4600 3100 50  0001 C CNN
F 3 "~" H 4600 3100 50  0001 C CNN
	1    4600 3100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5E63E891
P 3800 4200
AR Path="/5E63399D/5E63E891" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E63E891" Ref="SW2"  Part="1" 
AR Path="/5E6426EF/5E63E891" Ref="SW?"  Part="1" 
F 0 "SW2" H 3800 4485 50  0000 C CNN
F 1 "SW_Push" H 3800 4394 50  0000 C CNN
F 2 "" H 3800 4400 50  0001 C CNN
F 3 "~" H 3800 4400 50  0001 C CNN
	1    3800 4200
	1    0    0    -1  
$EndComp
Text GLabel 3050 4200 0    50   Input ~ 0
5v
Wire Wire Line
	4600 3200 4200 3200
Wire Wire Line
	3900 3200 3550 3200
Wire Wire Line
	3350 3200 3000 3200
Wire Wire Line
	3000 3200 3000 3500
Wire Wire Line
	3050 4200 3600 4200
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5E6354B1
P 4600 4100
AR Path="/5E63399D/5E6354B1" Ref="H?"  Part="1" 
AR Path="/5E634176/5E6354B1" Ref="H4"  Part="1" 
AR Path="/5E6426EF/5E6354B1" Ref="H?"  Part="1" 
F 0 "H4" H 4700 4149 50  0000 L CNN
F 1 "Solder_Pad" H 4700 4058 50  0000 L CNN
F 2 "" H 4600 4100 50  0001 C CNN
F 3 "~" H 4600 4100 50  0001 C CNN
	1    4600 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4200 4600 4200
$EndSCHEMATC
