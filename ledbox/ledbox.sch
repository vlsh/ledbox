EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLedger 17000 11000
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp "Firefly"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E647A6D
P 850 850
AR Path="/5E63399D/5E647A6D" Ref="R?"  Part="1" 
AR Path="/5E634176/5E647A6D" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E647A6D" Ref="R?"  Part="1" 
AR Path="/5E647A6D" Ref="R1"  Part="1" 
F 0 "R1" V 645 850 50  0000 C CNN
F 1 "120 Ohm" V 736 850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 850 50  0001 C CNN "Description"
F 5 "C149652" H 850 850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 850 50  0001 C CNN "MPN"
	1    850  850 
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical H?
U 1 1 5E647A73
P 1450 750
AR Path="/5E63399D/5E647A73" Ref="H?"  Part="1" 
AR Path="/5E634176/5E647A73" Ref="H?"  Part="1" 
AR Path="/5E6426EF/5E647A73" Ref="H?"  Part="1" 
AR Path="/5E647A73" Ref="A1"  Part="1" 
F 0 "A1" H 1550 799 50  0000 L CNN
F 1 "Solder_Pad" H 1550 708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 750 50  0001 C CNN
F 3 "~" H 1450 750 50  0001 C CNN
	1    1450 750 
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E647A79
P 1100 1350
AR Path="/5E63399D/5E647A79" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E647A79" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E647A79" Ref="SW?"  Part="1" 
AR Path="/5E647A79" Ref="SW1"  Part="1" 
F 0 "SW1" H 1100 1635 50  0000 C CNN
F 1 "SW_Push" H 1100 1544 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 1550 50  0001 C CNN
F 3 "~" H 1100 1550 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 1350 50  0001 C CNN "MPN"
	1    1100 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 850  1300 850 
Wire Wire Line
	1000 850  950  850 
Wire Wire Line
	750  850  650  850 
Wire Wire Line
	650  850  650  950 
Wire Wire Line
	700  1350 900  1350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical H?
U 1 1 5E647A85
P 1450 1250
AR Path="/5E63399D/5E647A85" Ref="H?"  Part="1" 
AR Path="/5E634176/5E647A85" Ref="H?"  Part="1" 
AR Path="/5E6426EF/5E647A85" Ref="H?"  Part="1" 
AR Path="/5E647A85" Ref="B1"  Part="1" 
F 0 "B1" H 1550 1299 50  0000 L CNN
F 1 "Solder_Pad" H 1550 1208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 1250 50  0001 C CNN
F 3 "~" H 1450 1250 50  0001 C CNN
	1    1450 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 1350 1450 1350
$Comp
L dk_LED:LG_R971-KN-1 D1
U 1 1 5E728300
P 1100 850
F 0 "D1" H 1050 603 60  0000 C CNN
F 1 "NCD0805O1" H 1050 709 60  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 1050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 1150 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 1250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 1350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 1450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 1550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 1650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 1750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 1850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 1950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 2050 60  0001 L CNN "Status"
F 13 "C84262" H 1100 850 50  0001 C CNN "LCSC"
	1    1100 850 
	-1   0    0    1   
$EndComp
Text Label 700  1350 2    50   ~ 0
VOUT
Text Label 650  950  2    50   ~ 0
GND
Text Label 15150 8700 0    50   ~ 0
VOUT
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical H1
U 1 1 5E73D342
P 15700 8700
F 0 "H1" H 15800 8749 50  0000 L CNN
F 1 "MountingHole_Pad" H 15800 8658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 15700 8700 50  0001 C CNN
F 3 "~" H 15700 8700 50  0001 C CNN
	1    15700 8700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E72D499
P 850 1800
AR Path="/5E63399D/5E72D499" Ref="R?"  Part="1" 
AR Path="/5E634176/5E72D499" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E72D499" Ref="R?"  Part="1" 
AR Path="/5E72D499" Ref="R2"  Part="1" 
F 0 "R2" V 645 1800 50  0000 C CNN
F 1 "120 Ohm" V 736 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 1800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 1800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 1800 50  0001 C CNN "Description"
F 5 "C149652" H 850 1800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 1800 50  0001 C CNN "MPN"
	1    850  1800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical H?
U 1 1 5E72D4A3
P 1450 1700
AR Path="/5E63399D/5E72D4A3" Ref="H?"  Part="1" 
AR Path="/5E634176/5E72D4A3" Ref="H?"  Part="1" 
AR Path="/5E6426EF/5E72D4A3" Ref="H?"  Part="1" 
AR Path="/5E72D4A3" Ref="A2"  Part="1" 
F 0 "A2" H 1550 1749 50  0000 L CNN
F 1 "Solder_Pad" H 1550 1658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 1700 50  0001 C CNN
F 3 "~" H 1450 1700 50  0001 C CNN
	1    1450 1700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E72D4AD
P 1100 2300
AR Path="/5E63399D/5E72D4AD" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E72D4AD" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E72D4AD" Ref="SW?"  Part="1" 
AR Path="/5E72D4AD" Ref="SW2"  Part="1" 
F 0 "SW2" H 1100 2585 50  0000 C CNN
F 1 "SW_Push" H 1100 2494 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 2500 50  0001 C CNN
F 3 "~" H 1100 2500 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 2300 50  0001 C CNN "MPN"
	1    1100 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 1800 1300 1800
Wire Wire Line
	1000 1800 950  1800
Wire Wire Line
	750  1800 650  1800
Wire Wire Line
	650  1800 650  1900
Wire Wire Line
	700  2300 900  2300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical H?
U 1 1 5E72D4BC
P 1450 2200
AR Path="/5E63399D/5E72D4BC" Ref="H?"  Part="1" 
AR Path="/5E634176/5E72D4BC" Ref="H?"  Part="1" 
AR Path="/5E6426EF/5E72D4BC" Ref="H?"  Part="1" 
AR Path="/5E72D4BC" Ref="B2"  Part="1" 
F 0 "B2" H 1550 2249 50  0000 L CNN
F 1 "Solder_Pad" H 1550 2158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 2200 50  0001 C CNN
F 3 "~" H 1450 2200 50  0001 C CNN
	1    1450 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2300 1450 2300
$Comp
L dk_LED:LG_R971-KN-1 D2
U 1 1 5E72D4D0
P 1100 1800
F 0 "D2" H 1050 1553 60  0000 C CNN
F 1 "NCD0805O1" H 1050 1659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 2000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 2100 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 2200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 2300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 2400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 2500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 2600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 2700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 2800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 2900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 3000 60  0001 L CNN "Status"
F 13 "C84262" H 1100 1800 50  0001 C CNN "LCSC"
	1    1100 1800
	-1   0    0    1   
$EndComp
Text Label 700  2300 2    50   ~ 0
VOUT
Text Label 650  1900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E741ED1
P 850 2800
AR Path="/5E63399D/5E741ED1" Ref="R?"  Part="1" 
AR Path="/5E634176/5E741ED1" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E741ED1" Ref="R?"  Part="1" 
AR Path="/5E741ED1" Ref="R3"  Part="1" 
F 0 "R3" V 645 2800 50  0000 C CNN
F 1 "120 Ohm" V 736 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 2800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 2800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 2800 50  0001 C CNN "Description"
F 5 "C149652" H 850 2800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 2800 50  0001 C CNN "MPN"
	1    850  2800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E741EDB
P 1450 2700
AR Path="/5E63399D/5E741EDB" Ref="A?"  Part="1" 
AR Path="/5E634176/5E741EDB" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E741EDB" Ref="A?"  Part="1" 
AR Path="/5E741EDB" Ref="A3"  Part="1" 
F 0 "A3" H 1550 2749 50  0000 L CNN
F 1 "Solder_Pad" H 1550 2658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 2700 50  0001 C CNN
F 3 "~" H 1450 2700 50  0001 C CNN
	1    1450 2700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E741EE5
P 1100 3300
AR Path="/5E63399D/5E741EE5" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E741EE5" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E741EE5" Ref="SW?"  Part="1" 
AR Path="/5E741EE5" Ref="SW3"  Part="1" 
F 0 "SW3" H 1100 3585 50  0000 C CNN
F 1 "SW_Push" H 1100 3494 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 3500 50  0001 C CNN
F 3 "~" H 1100 3500 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 3300 50  0001 C CNN "MPN"
	1    1100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2800 1300 2800
Wire Wire Line
	1000 2800 950  2800
Wire Wire Line
	750  2800 650  2800
Wire Wire Line
	650  2800 650  2900
Wire Wire Line
	700  3300 900  3300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E741EF4
P 1450 3200
AR Path="/5E63399D/5E741EF4" Ref="B?"  Part="1" 
AR Path="/5E634176/5E741EF4" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E741EF4" Ref="B?"  Part="1" 
AR Path="/5E741EF4" Ref="B3"  Part="1" 
F 0 "B3" H 1550 3249 50  0000 L CNN
F 1 "Solder_Pad" H 1550 3158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 3200 50  0001 C CNN
F 3 "~" H 1450 3200 50  0001 C CNN
	1    1450 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 3300 1450 3300
$Comp
L dk_LED:LG_R971-KN-1 D3
U 1 1 5E741F08
P 1100 2800
F 0 "D3" H 1050 2553 60  0000 C CNN
F 1 "NCD0805O1" H 1050 2659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 3000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 3100 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 3200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 3300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 3400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 3500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 3600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 3700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 3800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 3900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 4000 60  0001 L CNN "Status"
F 13 "C84262" H 1100 2800 50  0001 C CNN "LCSC"
	1    1100 2800
	-1   0    0    1   
$EndComp
Text Label 700  3300 2    50   ~ 0
VOUT
Text Label 650  2900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E7455BC
P 850 3850
AR Path="/5E63399D/5E7455BC" Ref="R?"  Part="1" 
AR Path="/5E634176/5E7455BC" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E7455BC" Ref="R?"  Part="1" 
AR Path="/5E7455BC" Ref="R4"  Part="1" 
F 0 "R4" V 645 3850 50  0000 C CNN
F 1 "120 Ohm" V 736 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 3850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 3850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 3850 50  0001 C CNN "Description"
F 5 "C149652" H 850 3850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 3850 50  0001 C CNN "MPN"
	1    850  3850
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E7455C6
P 1450 3750
AR Path="/5E63399D/5E7455C6" Ref="A?"  Part="1" 
AR Path="/5E634176/5E7455C6" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E7455C6" Ref="A?"  Part="1" 
AR Path="/5E7455C6" Ref="A4"  Part="1" 
F 0 "A4" H 1550 3799 50  0000 L CNN
F 1 "Solder_Pad" H 1550 3708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 3750 50  0001 C CNN
F 3 "~" H 1450 3750 50  0001 C CNN
	1    1450 3750
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E7455D0
P 1100 4350
AR Path="/5E63399D/5E7455D0" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E7455D0" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E7455D0" Ref="SW?"  Part="1" 
AR Path="/5E7455D0" Ref="SW4"  Part="1" 
F 0 "SW4" H 1100 4635 50  0000 C CNN
F 1 "SW_Push" H 1100 4544 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 4550 50  0001 C CNN
F 3 "~" H 1100 4550 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 4350 50  0001 C CNN "MPN"
	1    1100 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3850 1300 3850
Wire Wire Line
	1000 3850 950  3850
Wire Wire Line
	750  3850 650  3850
Wire Wire Line
	650  3850 650  3950
Wire Wire Line
	700  4350 900  4350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E7455DF
P 1450 4250
AR Path="/5E63399D/5E7455DF" Ref="B?"  Part="1" 
AR Path="/5E634176/5E7455DF" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E7455DF" Ref="B?"  Part="1" 
AR Path="/5E7455DF" Ref="B4"  Part="1" 
F 0 "B4" H 1550 4299 50  0000 L CNN
F 1 "Solder_Pad" H 1550 4208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 4250 50  0001 C CNN
F 3 "~" H 1450 4250 50  0001 C CNN
	1    1450 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 4350 1450 4350
$Comp
L dk_LED:LG_R971-KN-1 D4
U 1 1 5E7455F3
P 1100 3850
F 0 "D4" H 1050 3603 60  0000 C CNN
F 1 "NCD0805O1" H 1050 3709 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 4050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 4150 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 4350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 4450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 4550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 4850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 5050 60  0001 L CNN "Status"
F 13 "C84262" H 1100 3850 50  0001 C CNN "LCSC"
	1    1100 3850
	-1   0    0    1   
$EndComp
Text Label 700  4350 2    50   ~ 0
VOUT
Text Label 650  3950 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E748C49
P 850 4900
AR Path="/5E63399D/5E748C49" Ref="R?"  Part="1" 
AR Path="/5E634176/5E748C49" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E748C49" Ref="R?"  Part="1" 
AR Path="/5E748C49" Ref="R5"  Part="1" 
F 0 "R5" V 645 4900 50  0000 C CNN
F 1 "120 Ohm" V 736 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 4900 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 4900 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 4900 50  0001 C CNN "Description"
F 5 "C149652" H 850 4900 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 4900 50  0001 C CNN "MPN"
	1    850  4900
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E748C53
P 1450 4800
AR Path="/5E63399D/5E748C53" Ref="A?"  Part="1" 
AR Path="/5E634176/5E748C53" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E748C53" Ref="A?"  Part="1" 
AR Path="/5E748C53" Ref="A5"  Part="1" 
F 0 "A5" H 1550 4849 50  0000 L CNN
F 1 "Solder_Pad" H 1550 4758 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 4800 50  0001 C CNN
F 3 "~" H 1450 4800 50  0001 C CNN
	1    1450 4800
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E748C5D
P 1100 5400
AR Path="/5E63399D/5E748C5D" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E748C5D" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E748C5D" Ref="SW?"  Part="1" 
AR Path="/5E748C5D" Ref="SW5"  Part="1" 
F 0 "SW5" H 1100 5685 50  0000 C CNN
F 1 "SW_Push" H 1100 5594 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 5600 50  0001 C CNN
F 3 "~" H 1100 5600 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 5400 50  0001 C CNN "MPN"
	1    1100 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 4900 1300 4900
Wire Wire Line
	1000 4900 950  4900
Wire Wire Line
	750  4900 650  4900
Wire Wire Line
	650  4900 650  5000
Wire Wire Line
	700  5400 900  5400
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E748C6C
P 1450 5300
AR Path="/5E63399D/5E748C6C" Ref="B?"  Part="1" 
AR Path="/5E634176/5E748C6C" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E748C6C" Ref="B?"  Part="1" 
AR Path="/5E748C6C" Ref="B5"  Part="1" 
F 0 "B5" H 1550 5349 50  0000 L CNN
F 1 "Solder_Pad" H 1550 5258 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 5300 50  0001 C CNN
F 3 "~" H 1450 5300 50  0001 C CNN
	1    1450 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5400 1450 5400
$Comp
L dk_LED:LG_R971-KN-1 D5
U 1 1 5E748C80
P 1100 4900
F 0 "D5" H 1050 4653 60  0000 C CNN
F 1 "NCD0805O1" H 1050 4759 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 5100 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 5200 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 5300 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 5400 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 5500 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 5600 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 5700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 5800 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 5900 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 6000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 6100 60  0001 L CNN "Status"
F 13 "C84262" H 1100 4900 50  0001 C CNN "LCSC"
	1    1100 4900
	-1   0    0    1   
$EndComp
Text Label 700  5400 2    50   ~ 0
VOUT
Text Label 650  5000 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E74BDB3
P 850 5950
AR Path="/5E63399D/5E74BDB3" Ref="R?"  Part="1" 
AR Path="/5E634176/5E74BDB3" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E74BDB3" Ref="R?"  Part="1" 
AR Path="/5E74BDB3" Ref="R6"  Part="1" 
F 0 "R6" V 645 5950 50  0000 C CNN
F 1 "120 Ohm" V 736 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 5950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 5950 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 5950 50  0001 C CNN "Description"
F 5 "C149652" H 850 5950 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 5950 50  0001 C CNN "MPN"
	1    850  5950
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E74BDBD
P 1450 5850
AR Path="/5E63399D/5E74BDBD" Ref="A?"  Part="1" 
AR Path="/5E634176/5E74BDBD" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E74BDBD" Ref="A?"  Part="1" 
AR Path="/5E74BDBD" Ref="A6"  Part="1" 
F 0 "A6" H 1550 5899 50  0000 L CNN
F 1 "Solder_Pad" H 1550 5808 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 5850 50  0001 C CNN
F 3 "~" H 1450 5850 50  0001 C CNN
	1    1450 5850
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E74BDC7
P 1100 6450
AR Path="/5E63399D/5E74BDC7" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E74BDC7" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E74BDC7" Ref="SW?"  Part="1" 
AR Path="/5E74BDC7" Ref="SW6"  Part="1" 
F 0 "SW6" H 1100 6735 50  0000 C CNN
F 1 "SW_Push" H 1100 6644 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 6650 50  0001 C CNN
F 3 "~" H 1100 6650 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 6450 50  0001 C CNN "MPN"
	1    1100 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 5950 1300 5950
Wire Wire Line
	1000 5950 950  5950
Wire Wire Line
	750  5950 650  5950
Wire Wire Line
	650  5950 650  6050
Wire Wire Line
	700  6450 900  6450
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E74BDD6
P 1450 6350
AR Path="/5E63399D/5E74BDD6" Ref="B?"  Part="1" 
AR Path="/5E634176/5E74BDD6" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E74BDD6" Ref="B?"  Part="1" 
AR Path="/5E74BDD6" Ref="B6"  Part="1" 
F 0 "B6" H 1550 6399 50  0000 L CNN
F 1 "Solder_Pad" H 1550 6308 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 6350 50  0001 C CNN
F 3 "~" H 1450 6350 50  0001 C CNN
	1    1450 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 6450 1450 6450
$Comp
L dk_LED:LG_R971-KN-1 D6
U 1 1 5E74BDEA
P 1100 5950
F 0 "D6" H 1050 5703 60  0000 C CNN
F 1 "NCD0805O1" H 1050 5809 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 6150 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 6250 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 6350 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 6450 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 6550 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 6650 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 6750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 6850 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 6950 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 7050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 7150 60  0001 L CNN "Status"
F 13 "C84262" H 1100 5950 50  0001 C CNN "LCSC"
	1    1100 5950
	-1   0    0    1   
$EndComp
Text Label 700  6450 2    50   ~ 0
VOUT
Text Label 650  6050 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E74EFD7
P 850 7000
AR Path="/5E63399D/5E74EFD7" Ref="R?"  Part="1" 
AR Path="/5E634176/5E74EFD7" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E74EFD7" Ref="R?"  Part="1" 
AR Path="/5E74EFD7" Ref="R7"  Part="1" 
F 0 "R7" V 645 7000 50  0000 C CNN
F 1 "120 Ohm" V 736 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 7000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 7000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 7000 50  0001 C CNN "Description"
F 5 "C149652" H 850 7000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 7000 50  0001 C CNN "MPN"
	1    850  7000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E74EFE1
P 1450 6900
AR Path="/5E63399D/5E74EFE1" Ref="A?"  Part="1" 
AR Path="/5E634176/5E74EFE1" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E74EFE1" Ref="A?"  Part="1" 
AR Path="/5E74EFE1" Ref="A7"  Part="1" 
F 0 "A7" H 1550 6949 50  0000 L CNN
F 1 "Solder_Pad" H 1550 6858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 6900 50  0001 C CNN
F 3 "~" H 1450 6900 50  0001 C CNN
	1    1450 6900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E74EFEB
P 1100 7500
AR Path="/5E63399D/5E74EFEB" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E74EFEB" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E74EFEB" Ref="SW?"  Part="1" 
AR Path="/5E74EFEB" Ref="SW7"  Part="1" 
F 0 "SW7" H 1100 7785 50  0000 C CNN
F 1 "SW_Push" H 1100 7694 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 7700 50  0001 C CNN
F 3 "~" H 1100 7700 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 7500 50  0001 C CNN "MPN"
	1    1100 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 7000 1300 7000
Wire Wire Line
	1000 7000 950  7000
Wire Wire Line
	750  7000 650  7000
Wire Wire Line
	650  7000 650  7100
Wire Wire Line
	700  7500 900  7500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E74EFFA
P 1450 7400
AR Path="/5E63399D/5E74EFFA" Ref="B?"  Part="1" 
AR Path="/5E634176/5E74EFFA" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E74EFFA" Ref="B?"  Part="1" 
AR Path="/5E74EFFA" Ref="B7"  Part="1" 
F 0 "B7" H 1550 7449 50  0000 L CNN
F 1 "Solder_Pad" H 1550 7358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 7400 50  0001 C CNN
F 3 "~" H 1450 7400 50  0001 C CNN
	1    1450 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 7500 1450 7500
$Comp
L dk_LED:LG_R971-KN-1 D7
U 1 1 5E74F00E
P 1100 7000
F 0 "D7" H 1050 6753 60  0000 C CNN
F 1 "NCD0805O1" H 1050 6859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 7200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 7300 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 7400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 7500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 7600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 7700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 7800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 7900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 8000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 8100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 8200 60  0001 L CNN "Status"
F 13 "C84262" H 1100 7000 50  0001 C CNN "LCSC"
	1    1100 7000
	-1   0    0    1   
$EndComp
Text Label 700  7500 2    50   ~ 0
VOUT
Text Label 650  7100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E752AE2
P 850 8000
AR Path="/5E63399D/5E752AE2" Ref="R?"  Part="1" 
AR Path="/5E634176/5E752AE2" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E752AE2" Ref="R?"  Part="1" 
AR Path="/5E752AE2" Ref="R8"  Part="1" 
F 0 "R8" V 645 8000 50  0000 C CNN
F 1 "120 Ohm" V 736 8000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 8000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 8000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 8000 50  0001 C CNN "Description"
F 5 "C149652" H 850 8000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 8000 50  0001 C CNN "MPN"
	1    850  8000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E752AEC
P 1450 7900
AR Path="/5E63399D/5E752AEC" Ref="A?"  Part="1" 
AR Path="/5E634176/5E752AEC" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E752AEC" Ref="A?"  Part="1" 
AR Path="/5E752AEC" Ref="A8"  Part="1" 
F 0 "A8" H 1550 7949 50  0000 L CNN
F 1 "Solder_Pad" H 1550 7858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 7900 50  0001 C CNN
F 3 "~" H 1450 7900 50  0001 C CNN
	1    1450 7900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E752AF6
P 1100 8500
AR Path="/5E63399D/5E752AF6" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E752AF6" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E752AF6" Ref="SW?"  Part="1" 
AR Path="/5E752AF6" Ref="SW8"  Part="1" 
F 0 "SW8" H 1100 8785 50  0000 C CNN
F 1 "SW_Push" H 1100 8694 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 8700 50  0001 C CNN
F 3 "~" H 1100 8700 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 8500 50  0001 C CNN "MPN"
	1    1100 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 8000 1300 8000
Wire Wire Line
	1000 8000 950  8000
Wire Wire Line
	750  8000 650  8000
Wire Wire Line
	650  8000 650  8100
Wire Wire Line
	700  8500 900  8500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E752B05
P 1450 8400
AR Path="/5E63399D/5E752B05" Ref="B?"  Part="1" 
AR Path="/5E634176/5E752B05" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E752B05" Ref="B?"  Part="1" 
AR Path="/5E752B05" Ref="B8"  Part="1" 
F 0 "B8" H 1550 8449 50  0000 L CNN
F 1 "Solder_Pad" H 1550 8358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 8400 50  0001 C CNN
F 3 "~" H 1450 8400 50  0001 C CNN
	1    1450 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 8500 1450 8500
$Comp
L dk_LED:LG_R971-KN-1 D8
U 1 1 5E752B19
P 1100 8000
F 0 "D8" H 1050 7753 60  0000 C CNN
F 1 "NCD0805O1" H 1050 7859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 8200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 8300 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 8400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 8500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 8600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 8700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 8800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 8900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 9000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 9100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 9200 60  0001 L CNN "Status"
F 13 "C84262" H 1100 8000 50  0001 C CNN "LCSC"
	1    1100 8000
	-1   0    0    1   
$EndComp
Text Label 700  8500 2    50   ~ 0
VOUT
Text Label 650  8100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E755FBB
P 850 9050
AR Path="/5E63399D/5E755FBB" Ref="R?"  Part="1" 
AR Path="/5E634176/5E755FBB" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E755FBB" Ref="R?"  Part="1" 
AR Path="/5E755FBB" Ref="R9"  Part="1" 
F 0 "R9" V 645 9050 50  0000 C CNN
F 1 "120 Ohm" V 736 9050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 850 9050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 850 9050 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 850 9050 50  0001 C CNN "Description"
F 5 "C149652" H 850 9050 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 850 9050 50  0001 C CNN "MPN"
	1    850  9050
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E755FC5
P 1450 8950
AR Path="/5E63399D/5E755FC5" Ref="A?"  Part="1" 
AR Path="/5E634176/5E755FC5" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E755FC5" Ref="A?"  Part="1" 
AR Path="/5E755FC5" Ref="A9"  Part="1" 
F 0 "A9" H 1550 8999 50  0000 L CNN
F 1 "Solder_Pad" H 1550 8908 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 8950 50  0001 C CNN
F 3 "~" H 1450 8950 50  0001 C CNN
	1    1450 8950
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E755FCF
P 1100 9550
AR Path="/5E63399D/5E755FCF" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E755FCF" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E755FCF" Ref="SW?"  Part="1" 
AR Path="/5E755FCF" Ref="SW9"  Part="1" 
F 0 "SW9" H 1100 9835 50  0000 C CNN
F 1 "SW_Push" H 1100 9744 50  0000 C CNN
F 2 "footprints:TL1105" H 1100 9750 50  0001 C CNN
F 3 "~" H 1100 9750 50  0001 C CNN
F 4 "TL1105LF250Q" H 1100 9550 50  0001 C CNN "MPN"
	1    1100 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 9050 1300 9050
Wire Wire Line
	1000 9050 950  9050
Wire Wire Line
	750  9050 650  9050
Wire Wire Line
	650  9050 650  9150
Wire Wire Line
	700  9550 900  9550
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E755FDE
P 1450 9450
AR Path="/5E63399D/5E755FDE" Ref="B?"  Part="1" 
AR Path="/5E634176/5E755FDE" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E755FDE" Ref="B?"  Part="1" 
AR Path="/5E755FDE" Ref="B9"  Part="1" 
F 0 "B9" H 1550 9499 50  0000 L CNN
F 1 "Solder_Pad" H 1550 9408 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 1450 9450 50  0001 C CNN
F 3 "~" H 1450 9450 50  0001 C CNN
	1    1450 9450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 9550 1450 9550
$Comp
L dk_LED:LG_R971-KN-1 D9
U 1 1 5E755FF2
P 1100 9050
F 0 "D9" H 1050 8803 60  0000 C CNN
F 1 "NCD0805O1" H 1050 8909 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 1300 9250 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 1300 9350 60  0001 L CNN
F 4 "475-1410-1-ND" H 1300 9450 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 1300 9550 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 1300 9650 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 1300 9750 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1300 9850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 1300 9950 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 1300 10050 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 1300 10150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1300 10250 60  0001 L CNN "Status"
F 13 "C84262" H 1100 9050 50  0001 C CNN "LCSC"
	1    1100 9050
	-1   0    0    1   
$EndComp
Text Label 700  9550 2    50   ~ 0
VOUT
Text Label 650  9150 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E7596AB
P 2450 800
AR Path="/5E63399D/5E7596AB" Ref="R?"  Part="1" 
AR Path="/5E634176/5E7596AB" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E7596AB" Ref="R?"  Part="1" 
AR Path="/5E7596AB" Ref="R10"  Part="1" 
F 0 "R10" V 2245 800 50  0000 C CNN
F 1 "120 Ohm" V 2336 800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 800 50  0001 C CNN "Description"
F 5 "C149652" H 2450 800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 800 50  0001 C CNN "MPN"
	1    2450 800 
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E7596B5
P 3050 700
AR Path="/5E63399D/5E7596B5" Ref="A?"  Part="1" 
AR Path="/5E634176/5E7596B5" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E7596B5" Ref="A?"  Part="1" 
AR Path="/5E7596B5" Ref="A10"  Part="1" 
F 0 "A10" H 3150 749 50  0000 L CNN
F 1 "Solder_Pad" H 3150 658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 700 50  0001 C CNN
F 3 "~" H 3050 700 50  0001 C CNN
	1    3050 700 
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E7596BF
P 2700 1300
AR Path="/5E63399D/5E7596BF" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E7596BF" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E7596BF" Ref="SW?"  Part="1" 
AR Path="/5E7596BF" Ref="SW10"  Part="1" 
F 0 "SW10" H 2700 1585 50  0000 C CNN
F 1 "SW_Push" H 2700 1494 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 1500 50  0001 C CNN
F 3 "~" H 2700 1500 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 1300 50  0001 C CNN "MPN"
	1    2700 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 800  2900 800 
Wire Wire Line
	2600 800  2550 800 
Wire Wire Line
	2350 800  2250 800 
Wire Wire Line
	2250 800  2250 900 
Wire Wire Line
	2300 1300 2500 1300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E7596CE
P 3050 1200
AR Path="/5E63399D/5E7596CE" Ref="B?"  Part="1" 
AR Path="/5E634176/5E7596CE" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E7596CE" Ref="B?"  Part="1" 
AR Path="/5E7596CE" Ref="B10"  Part="1" 
F 0 "B10" H 3150 1249 50  0000 L CNN
F 1 "Solder_Pad" H 3150 1158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 1200 50  0001 C CNN
F 3 "~" H 3050 1200 50  0001 C CNN
	1    3050 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1300 3050 1300
$Comp
L dk_LED:LG_R971-KN-1 D10
U 1 1 5E7596E2
P 2700 800
F 0 "D10" H 2650 553 60  0000 C CNN
F 1 "NCD0805O1" H 2650 659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 1000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 1100 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 1200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 1300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 1400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 1500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 1600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 1700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 1800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 1900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 2000 60  0001 L CNN "Status"
F 13 "C84262" H 2700 800 50  0001 C CNN "LCSC"
	1    2700 800 
	-1   0    0    1   
$EndComp
Text Label 2300 1300 2    50   ~ 0
VOUT
Text Label 2250 900  2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E76704A
P 2450 1750
AR Path="/5E63399D/5E76704A" Ref="R?"  Part="1" 
AR Path="/5E634176/5E76704A" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E76704A" Ref="R?"  Part="1" 
AR Path="/5E76704A" Ref="R11"  Part="1" 
F 0 "R11" V 2245 1750 50  0000 C CNN
F 1 "120 Ohm" V 2336 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 1750 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 1750 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 1750 50  0001 C CNN "Description"
F 5 "C149652" H 2450 1750 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 1750 50  0001 C CNN "MPN"
	1    2450 1750
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E767054
P 3050 1650
AR Path="/5E63399D/5E767054" Ref="A?"  Part="1" 
AR Path="/5E634176/5E767054" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E767054" Ref="A?"  Part="1" 
AR Path="/5E767054" Ref="A11"  Part="1" 
F 0 "A11" H 3150 1699 50  0000 L CNN
F 1 "Solder_Pad" H 3150 1608 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 1650 50  0001 C CNN
F 3 "~" H 3050 1650 50  0001 C CNN
	1    3050 1650
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E76705E
P 2700 2250
AR Path="/5E63399D/5E76705E" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E76705E" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E76705E" Ref="SW?"  Part="1" 
AR Path="/5E76705E" Ref="SW11"  Part="1" 
F 0 "SW11" H 2700 2535 50  0000 C CNN
F 1 "SW_Push" H 2700 2444 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 2450 50  0001 C CNN
F 3 "~" H 2700 2450 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 2250 50  0001 C CNN "MPN"
	1    2700 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 1750 2900 1750
Wire Wire Line
	2600 1750 2550 1750
Wire Wire Line
	2350 1750 2250 1750
Wire Wire Line
	2250 1750 2250 1850
Wire Wire Line
	2300 2250 2500 2250
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E76706D
P 3050 2150
AR Path="/5E63399D/5E76706D" Ref="B?"  Part="1" 
AR Path="/5E634176/5E76706D" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E76706D" Ref="B?"  Part="1" 
AR Path="/5E76706D" Ref="B11"  Part="1" 
F 0 "B11" H 3150 2199 50  0000 L CNN
F 1 "Solder_Pad" H 3150 2108 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 2150 50  0001 C CNN
F 3 "~" H 3050 2150 50  0001 C CNN
	1    3050 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2250 3050 2250
$Comp
L dk_LED:LG_R971-KN-1 D11
U 1 1 5E767081
P 2700 1750
F 0 "D11" H 2650 1503 60  0000 C CNN
F 1 "NCD0805O1" H 2650 1609 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 1950 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 2050 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 2150 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 2250 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 2350 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 2450 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 2550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 2650 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 2750 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 2850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 2950 60  0001 L CNN "Status"
F 13 "C84262" H 2700 1750 50  0001 C CNN "LCSC"
	1    2700 1750
	-1   0    0    1   
$EndComp
Text Label 2300 2250 2    50   ~ 0
VOUT
Text Label 2250 1850 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E76B529
P 2450 2800
AR Path="/5E63399D/5E76B529" Ref="R?"  Part="1" 
AR Path="/5E634176/5E76B529" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E76B529" Ref="R?"  Part="1" 
AR Path="/5E76B529" Ref="R12"  Part="1" 
F 0 "R12" V 2245 2800 50  0000 C CNN
F 1 "120 Ohm" V 2336 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 2800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 2800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 2800 50  0001 C CNN "Description"
F 5 "C149652" H 2450 2800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 2800 50  0001 C CNN "MPN"
	1    2450 2800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E76B533
P 3050 2700
AR Path="/5E63399D/5E76B533" Ref="A?"  Part="1" 
AR Path="/5E634176/5E76B533" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E76B533" Ref="A?"  Part="1" 
AR Path="/5E76B533" Ref="A12"  Part="1" 
F 0 "A12" H 3150 2749 50  0000 L CNN
F 1 "Solder_Pad" H 3150 2658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 2700 50  0001 C CNN
F 3 "~" H 3050 2700 50  0001 C CNN
	1    3050 2700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E76B53D
P 2700 3300
AR Path="/5E63399D/5E76B53D" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E76B53D" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E76B53D" Ref="SW?"  Part="1" 
AR Path="/5E76B53D" Ref="SW12"  Part="1" 
F 0 "SW12" H 2700 3585 50  0000 C CNN
F 1 "SW_Push" H 2700 3494 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 3500 50  0001 C CNN
F 3 "~" H 2700 3500 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 3300 50  0001 C CNN "MPN"
	1    2700 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2800 2900 2800
Wire Wire Line
	2600 2800 2550 2800
Wire Wire Line
	2350 2800 2250 2800
Wire Wire Line
	2250 2800 2250 2900
Wire Wire Line
	2300 3300 2500 3300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E76B54C
P 3050 3200
AR Path="/5E63399D/5E76B54C" Ref="B?"  Part="1" 
AR Path="/5E634176/5E76B54C" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E76B54C" Ref="B?"  Part="1" 
AR Path="/5E76B54C" Ref="B12"  Part="1" 
F 0 "B12" H 3150 3249 50  0000 L CNN
F 1 "Solder_Pad" H 3150 3158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 3200 50  0001 C CNN
F 3 "~" H 3050 3200 50  0001 C CNN
	1    3050 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3300 3050 3300
$Comp
L dk_LED:LG_R971-KN-1 D12
U 1 1 5E76B560
P 2700 2800
F 0 "D12" H 2650 2553 60  0000 C CNN
F 1 "NCD0805O1" H 2650 2659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 3000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 3100 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 3200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 3300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 3400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 3500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 3600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 3700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 3800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 3900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 4000 60  0001 L CNN "Status"
F 13 "C84262" H 2700 2800 50  0001 C CNN "LCSC"
	1    2700 2800
	-1   0    0    1   
$EndComp
Text Label 2300 3300 2    50   ~ 0
VOUT
Text Label 2250 2900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E76FC9D
P 2450 3850
AR Path="/5E63399D/5E76FC9D" Ref="R?"  Part="1" 
AR Path="/5E634176/5E76FC9D" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E76FC9D" Ref="R?"  Part="1" 
AR Path="/5E76FC9D" Ref="R13"  Part="1" 
F 0 "R13" V 2245 3850 50  0000 C CNN
F 1 "120 Ohm" V 2336 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 3850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 3850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 3850 50  0001 C CNN "Description"
F 5 "C149652" H 2450 3850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 3850 50  0001 C CNN "MPN"
	1    2450 3850
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E76FCA7
P 3050 3750
AR Path="/5E63399D/5E76FCA7" Ref="A?"  Part="1" 
AR Path="/5E634176/5E76FCA7" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E76FCA7" Ref="A?"  Part="1" 
AR Path="/5E76FCA7" Ref="A13"  Part="1" 
F 0 "A13" H 3150 3799 50  0000 L CNN
F 1 "Solder_Pad" H 3150 3708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 3750 50  0001 C CNN
F 3 "~" H 3050 3750 50  0001 C CNN
	1    3050 3750
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E76FCB1
P 2700 4350
AR Path="/5E63399D/5E76FCB1" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E76FCB1" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E76FCB1" Ref="SW?"  Part="1" 
AR Path="/5E76FCB1" Ref="SW13"  Part="1" 
F 0 "SW13" H 2700 4635 50  0000 C CNN
F 1 "SW_Push" H 2700 4544 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 4550 50  0001 C CNN
F 3 "~" H 2700 4550 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 4350 50  0001 C CNN "MPN"
	1    2700 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3850 2900 3850
Wire Wire Line
	2600 3850 2550 3850
Wire Wire Line
	2350 3850 2250 3850
Wire Wire Line
	2250 3850 2250 3950
Wire Wire Line
	2300 4350 2500 4350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E76FCC0
P 3050 4250
AR Path="/5E63399D/5E76FCC0" Ref="B?"  Part="1" 
AR Path="/5E634176/5E76FCC0" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E76FCC0" Ref="B?"  Part="1" 
AR Path="/5E76FCC0" Ref="B13"  Part="1" 
F 0 "B13" H 3150 4299 50  0000 L CNN
F 1 "Solder_Pad" H 3150 4208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 4250 50  0001 C CNN
F 3 "~" H 3050 4250 50  0001 C CNN
	1    3050 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4350 3050 4350
$Comp
L dk_LED:LG_R971-KN-1 D13
U 1 1 5E76FCD4
P 2700 3850
F 0 "D13" H 2650 3603 60  0000 C CNN
F 1 "NCD0805O1" H 2650 3709 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 4050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 4150 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 4350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 4450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 4550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 4850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 5050 60  0001 L CNN "Status"
F 13 "C84262" H 2700 3850 50  0001 C CNN "LCSC"
	1    2700 3850
	-1   0    0    1   
$EndComp
Text Label 2300 4350 2    50   ~ 0
VOUT
Text Label 2250 3950 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E781B74
P 2450 4900
AR Path="/5E63399D/5E781B74" Ref="R?"  Part="1" 
AR Path="/5E634176/5E781B74" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E781B74" Ref="R?"  Part="1" 
AR Path="/5E781B74" Ref="R14"  Part="1" 
F 0 "R14" V 2245 4900 50  0000 C CNN
F 1 "120 Ohm" V 2336 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 4900 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 4900 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 4900 50  0001 C CNN "Description"
F 5 "C149652" H 2450 4900 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 4900 50  0001 C CNN "MPN"
	1    2450 4900
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E781B7E
P 3050 4800
AR Path="/5E63399D/5E781B7E" Ref="A?"  Part="1" 
AR Path="/5E634176/5E781B7E" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E781B7E" Ref="A?"  Part="1" 
AR Path="/5E781B7E" Ref="A14"  Part="1" 
F 0 "A14" H 3150 4849 50  0000 L CNN
F 1 "Solder_Pad" H 3150 4758 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 4800 50  0001 C CNN
F 3 "~" H 3050 4800 50  0001 C CNN
	1    3050 4800
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E781B88
P 2700 5400
AR Path="/5E63399D/5E781B88" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E781B88" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E781B88" Ref="SW?"  Part="1" 
AR Path="/5E781B88" Ref="SW14"  Part="1" 
F 0 "SW14" H 2700 5685 50  0000 C CNN
F 1 "SW_Push" H 2700 5594 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 5600 50  0001 C CNN
F 3 "~" H 2700 5600 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 5400 50  0001 C CNN "MPN"
	1    2700 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4900 2900 4900
Wire Wire Line
	2600 4900 2550 4900
Wire Wire Line
	2350 4900 2250 4900
Wire Wire Line
	2250 4900 2250 5000
Wire Wire Line
	2300 5400 2500 5400
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E781B97
P 3050 5300
AR Path="/5E63399D/5E781B97" Ref="B?"  Part="1" 
AR Path="/5E634176/5E781B97" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E781B97" Ref="B?"  Part="1" 
AR Path="/5E781B97" Ref="B14"  Part="1" 
F 0 "B14" H 3150 5349 50  0000 L CNN
F 1 "Solder_Pad" H 3150 5258 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 5300 50  0001 C CNN
F 3 "~" H 3050 5300 50  0001 C CNN
	1    3050 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 5400 3050 5400
$Comp
L dk_LED:LG_R971-KN-1 D14
U 1 1 5E781BAB
P 2700 4900
F 0 "D14" H 2650 4653 60  0000 C CNN
F 1 "NCD0805O1" H 2650 4759 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 5100 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 5200 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 5300 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 5400 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 5500 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 5600 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 5700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 5800 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 5900 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 6000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 6100 60  0001 L CNN "Status"
F 13 "C84262" H 2700 4900 50  0001 C CNN "LCSC"
	1    2700 4900
	-1   0    0    1   
$EndComp
Text Label 2300 5400 2    50   ~ 0
VOUT
Text Label 2250 5000 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E78A3C7
P 2450 5950
AR Path="/5E63399D/5E78A3C7" Ref="R?"  Part="1" 
AR Path="/5E634176/5E78A3C7" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E78A3C7" Ref="R?"  Part="1" 
AR Path="/5E78A3C7" Ref="R15"  Part="1" 
F 0 "R15" V 2245 5950 50  0000 C CNN
F 1 "120 Ohm" V 2336 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 5950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 5950 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 5950 50  0001 C CNN "Description"
F 5 "C149652" H 2450 5950 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 5950 50  0001 C CNN "MPN"
	1    2450 5950
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E78A3D1
P 3050 5850
AR Path="/5E63399D/5E78A3D1" Ref="A?"  Part="1" 
AR Path="/5E634176/5E78A3D1" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E78A3D1" Ref="A?"  Part="1" 
AR Path="/5E78A3D1" Ref="A15"  Part="1" 
F 0 "A15" H 3150 5899 50  0000 L CNN
F 1 "Solder_Pad" H 3150 5808 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 5850 50  0001 C CNN
F 3 "~" H 3050 5850 50  0001 C CNN
	1    3050 5850
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E78A3DB
P 2700 6450
AR Path="/5E63399D/5E78A3DB" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E78A3DB" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E78A3DB" Ref="SW?"  Part="1" 
AR Path="/5E78A3DB" Ref="SW15"  Part="1" 
F 0 "SW15" H 2700 6735 50  0000 C CNN
F 1 "SW_Push" H 2700 6644 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 6650 50  0001 C CNN
F 3 "~" H 2700 6650 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 6450 50  0001 C CNN "MPN"
	1    2700 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 5950 2900 5950
Wire Wire Line
	2600 5950 2550 5950
Wire Wire Line
	2350 5950 2250 5950
Wire Wire Line
	2250 5950 2250 6050
Wire Wire Line
	2300 6450 2500 6450
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E78A3EA
P 3050 6350
AR Path="/5E63399D/5E78A3EA" Ref="B?"  Part="1" 
AR Path="/5E634176/5E78A3EA" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E78A3EA" Ref="B?"  Part="1" 
AR Path="/5E78A3EA" Ref="B15"  Part="1" 
F 0 "B15" H 3150 6399 50  0000 L CNN
F 1 "Solder_Pad" H 3150 6308 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 6350 50  0001 C CNN
F 3 "~" H 3050 6350 50  0001 C CNN
	1    3050 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 6450 3050 6450
$Comp
L dk_LED:LG_R971-KN-1 D15
U 1 1 5E78A3FE
P 2700 5950
F 0 "D15" H 2650 5703 60  0000 C CNN
F 1 "NCD0805O1" H 2650 5809 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 6150 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 6250 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 6350 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 6450 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 6550 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 6650 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 6750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 6850 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 6950 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 7050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 7150 60  0001 L CNN "Status"
F 13 "C84262" H 2700 5950 50  0001 C CNN "LCSC"
	1    2700 5950
	-1   0    0    1   
$EndComp
Text Label 2300 6450 2    50   ~ 0
VOUT
Text Label 2250 6050 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E7930D2
P 2450 7000
AR Path="/5E63399D/5E7930D2" Ref="R?"  Part="1" 
AR Path="/5E634176/5E7930D2" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E7930D2" Ref="R?"  Part="1" 
AR Path="/5E7930D2" Ref="R16"  Part="1" 
F 0 "R16" V 2245 7000 50  0000 C CNN
F 1 "120 Ohm" V 2336 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 7000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 7000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 7000 50  0001 C CNN "Description"
F 5 "C149652" H 2450 7000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 7000 50  0001 C CNN "MPN"
	1    2450 7000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E7930DC
P 3050 6900
AR Path="/5E63399D/5E7930DC" Ref="A?"  Part="1" 
AR Path="/5E634176/5E7930DC" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E7930DC" Ref="A?"  Part="1" 
AR Path="/5E7930DC" Ref="A16"  Part="1" 
F 0 "A16" H 3150 6949 50  0000 L CNN
F 1 "Solder_Pad" H 3150 6858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 6900 50  0001 C CNN
F 3 "~" H 3050 6900 50  0001 C CNN
	1    3050 6900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E7930E6
P 2700 7500
AR Path="/5E63399D/5E7930E6" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E7930E6" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E7930E6" Ref="SW?"  Part="1" 
AR Path="/5E7930E6" Ref="SW16"  Part="1" 
F 0 "SW16" H 2700 7785 50  0000 C CNN
F 1 "SW_Push" H 2700 7694 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 7700 50  0001 C CNN
F 3 "~" H 2700 7700 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 7500 50  0001 C CNN "MPN"
	1    2700 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 7000 2900 7000
Wire Wire Line
	2600 7000 2550 7000
Wire Wire Line
	2350 7000 2250 7000
Wire Wire Line
	2250 7000 2250 7100
Wire Wire Line
	2300 7500 2500 7500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E7930F5
P 3050 7400
AR Path="/5E63399D/5E7930F5" Ref="B?"  Part="1" 
AR Path="/5E634176/5E7930F5" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E7930F5" Ref="B?"  Part="1" 
AR Path="/5E7930F5" Ref="B16"  Part="1" 
F 0 "B16" H 3150 7449 50  0000 L CNN
F 1 "Solder_Pad" H 3150 7358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 7400 50  0001 C CNN
F 3 "~" H 3050 7400 50  0001 C CNN
	1    3050 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 7500 3050 7500
$Comp
L dk_LED:LG_R971-KN-1 D16
U 1 1 5E793109
P 2700 7000
F 0 "D16" H 2650 6753 60  0000 C CNN
F 1 "NCD0805O1" H 2650 6859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 7200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 7300 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 7400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 7500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 7600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 7700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 7800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 7900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 8000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 8100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 8200 60  0001 L CNN "Status"
F 13 "C84262" H 2700 7000 50  0001 C CNN "LCSC"
	1    2700 7000
	-1   0    0    1   
$EndComp
Text Label 2300 7500 2    50   ~ 0
VOUT
Text Label 2250 7100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E7994BE
P 2450 8000
AR Path="/5E63399D/5E7994BE" Ref="R?"  Part="1" 
AR Path="/5E634176/5E7994BE" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E7994BE" Ref="R?"  Part="1" 
AR Path="/5E7994BE" Ref="R17"  Part="1" 
F 0 "R17" V 2245 8000 50  0000 C CNN
F 1 "120 Ohm" V 2336 8000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 8000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 8000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 8000 50  0001 C CNN "Description"
F 5 "C149652" H 2450 8000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 8000 50  0001 C CNN "MPN"
	1    2450 8000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E7994C8
P 3050 7900
AR Path="/5E63399D/5E7994C8" Ref="A?"  Part="1" 
AR Path="/5E634176/5E7994C8" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E7994C8" Ref="A?"  Part="1" 
AR Path="/5E7994C8" Ref="A17"  Part="1" 
F 0 "A17" H 3150 7949 50  0000 L CNN
F 1 "Solder_Pad" H 3150 7858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 7900 50  0001 C CNN
F 3 "~" H 3050 7900 50  0001 C CNN
	1    3050 7900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E7994D2
P 2700 8500
AR Path="/5E63399D/5E7994D2" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E7994D2" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E7994D2" Ref="SW?"  Part="1" 
AR Path="/5E7994D2" Ref="SW17"  Part="1" 
F 0 "SW17" H 2700 8785 50  0000 C CNN
F 1 "SW_Push" H 2700 8694 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 8700 50  0001 C CNN
F 3 "~" H 2700 8700 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 8500 50  0001 C CNN "MPN"
	1    2700 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 8000 2900 8000
Wire Wire Line
	2600 8000 2550 8000
Wire Wire Line
	2350 8000 2250 8000
Wire Wire Line
	2250 8000 2250 8100
Wire Wire Line
	2300 8500 2500 8500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E7994E1
P 3050 8400
AR Path="/5E63399D/5E7994E1" Ref="B?"  Part="1" 
AR Path="/5E634176/5E7994E1" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E7994E1" Ref="B?"  Part="1" 
AR Path="/5E7994E1" Ref="B17"  Part="1" 
F 0 "B17" H 3150 8449 50  0000 L CNN
F 1 "Solder_Pad" H 3150 8358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 8400 50  0001 C CNN
F 3 "~" H 3050 8400 50  0001 C CNN
	1    3050 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 8500 3050 8500
$Comp
L dk_LED:LG_R971-KN-1 D17
U 1 1 5E7994F5
P 2700 8000
F 0 "D17" H 2650 7753 60  0000 C CNN
F 1 "NCD0805O1" H 2650 7859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 8200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 8300 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 8400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 8500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 8600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 8700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 8800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 8900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 9000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 9100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 9200 60  0001 L CNN "Status"
F 13 "C84262" H 2700 8000 50  0001 C CNN "LCSC"
	1    2700 8000
	-1   0    0    1   
$EndComp
Text Label 2300 8500 2    50   ~ 0
VOUT
Text Label 2250 8100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E7A43A2
P 2450 9050
AR Path="/5E63399D/5E7A43A2" Ref="R?"  Part="1" 
AR Path="/5E634176/5E7A43A2" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E7A43A2" Ref="R?"  Part="1" 
AR Path="/5E7A43A2" Ref="R18"  Part="1" 
F 0 "R18" V 2245 9050 50  0000 C CNN
F 1 "120 Ohm" V 2336 9050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2450 9050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 2450 9050 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 2450 9050 50  0001 C CNN "Description"
F 5 "C149652" H 2450 9050 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 2450 9050 50  0001 C CNN "MPN"
	1    2450 9050
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E7A43AC
P 3050 8950
AR Path="/5E63399D/5E7A43AC" Ref="A?"  Part="1" 
AR Path="/5E634176/5E7A43AC" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E7A43AC" Ref="A?"  Part="1" 
AR Path="/5E7A43AC" Ref="A18"  Part="1" 
F 0 "A18" H 3150 8999 50  0000 L CNN
F 1 "Solder_Pad" H 3150 8908 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 8950 50  0001 C CNN
F 3 "~" H 3050 8950 50  0001 C CNN
	1    3050 8950
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E7A43B6
P 2700 9550
AR Path="/5E63399D/5E7A43B6" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E7A43B6" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E7A43B6" Ref="SW?"  Part="1" 
AR Path="/5E7A43B6" Ref="SW18"  Part="1" 
F 0 "SW18" H 2700 9835 50  0000 C CNN
F 1 "SW_Push" H 2700 9744 50  0000 C CNN
F 2 "footprints:TL1105" H 2700 9750 50  0001 C CNN
F 3 "~" H 2700 9750 50  0001 C CNN
F 4 "TL1105LF250Q" H 2700 9550 50  0001 C CNN "MPN"
	1    2700 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 9050 2900 9050
Wire Wire Line
	2600 9050 2550 9050
Wire Wire Line
	2350 9050 2250 9050
Wire Wire Line
	2250 9050 2250 9150
Wire Wire Line
	2300 9550 2500 9550
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E7A43C5
P 3050 9450
AR Path="/5E63399D/5E7A43C5" Ref="B?"  Part="1" 
AR Path="/5E634176/5E7A43C5" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E7A43C5" Ref="B?"  Part="1" 
AR Path="/5E7A43C5" Ref="B18"  Part="1" 
F 0 "B18" H 3150 9499 50  0000 L CNN
F 1 "Solder_Pad" H 3150 9408 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 3050 9450 50  0001 C CNN
F 3 "~" H 3050 9450 50  0001 C CNN
	1    3050 9450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 9550 3050 9550
$Comp
L dk_LED:LG_R971-KN-1 D18
U 1 1 5E7A43D9
P 2700 9050
F 0 "D18" H 2650 8803 60  0000 C CNN
F 1 "NCD0805O1" H 2650 8909 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 2900 9250 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 2900 9350 60  0001 L CNN
F 4 "475-1410-1-ND" H 2900 9450 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 2900 9550 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 2900 9650 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 2900 9750 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 2900 9850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 2900 9950 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 2900 10050 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 2900 10150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2900 10250 60  0001 L CNN "Status"
F 13 "C84262" H 2700 9050 50  0001 C CNN "LCSC"
	1    2700 9050
	-1   0    0    1   
$EndComp
Text Label 2300 9550 2    50   ~ 0
VOUT
Text Label 2250 9150 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E80221D
P 4100 850
AR Path="/5E63399D/5E80221D" Ref="R?"  Part="1" 
AR Path="/5E634176/5E80221D" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E80221D" Ref="R?"  Part="1" 
AR Path="/5E80221D" Ref="R19"  Part="1" 
F 0 "R19" V 3895 850 50  0000 C CNN
F 1 "120 Ohm" V 3986 850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 850 50  0001 C CNN "Description"
F 5 "C149652" H 4100 850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 850 50  0001 C CNN "MPN"
	1    4100 850 
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E802227
P 4700 750
AR Path="/5E63399D/5E802227" Ref="A?"  Part="1" 
AR Path="/5E634176/5E802227" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E802227" Ref="A?"  Part="1" 
AR Path="/5E802227" Ref="A19"  Part="1" 
F 0 "A19" H 4800 799 50  0000 L CNN
F 1 "Solder_Pad" H 4800 708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 750 50  0001 C CNN
F 3 "~" H 4700 750 50  0001 C CNN
	1    4700 750 
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802231
P 4350 1350
AR Path="/5E63399D/5E802231" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802231" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802231" Ref="SW?"  Part="1" 
AR Path="/5E802231" Ref="SW19"  Part="1" 
F 0 "SW19" H 4350 1635 50  0000 C CNN
F 1 "SW_Push" H 4350 1544 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 1550 50  0001 C CNN
F 3 "~" H 4350 1550 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 1350 50  0001 C CNN "MPN"
	1    4350 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 850  4550 850 
Wire Wire Line
	4250 850  4200 850 
Wire Wire Line
	4000 850  3900 850 
Wire Wire Line
	3900 850  3900 950 
Wire Wire Line
	3950 1350 4150 1350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E802240
P 4700 1250
AR Path="/5E63399D/5E802240" Ref="B?"  Part="1" 
AR Path="/5E634176/5E802240" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E802240" Ref="B?"  Part="1" 
AR Path="/5E802240" Ref="B19"  Part="1" 
F 0 "B19" H 4800 1299 50  0000 L CNN
F 1 "Solder_Pad" H 4800 1208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 1250 50  0001 C CNN
F 3 "~" H 4700 1250 50  0001 C CNN
	1    4700 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1350 4700 1350
$Comp
L dk_LED:LG_R971-KN-1 D19
U 1 1 5E802254
P 4350 850
F 0 "D19" H 4300 603 60  0000 C CNN
F 1 "NCD0805O1" H 4300 709 60  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 1050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 1150 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 1250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 1350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 1450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 1550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 1650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 1750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 1850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 1950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 2050 60  0001 L CNN "Status"
F 13 "C84262" H 4350 850 50  0001 C CNN "LCSC"
	1    4350 850 
	-1   0    0    1   
$EndComp
Text Label 3950 1350 2    50   ~ 0
VOUT
Text Label 3900 950  2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E802260
P 4100 1800
AR Path="/5E63399D/5E802260" Ref="R?"  Part="1" 
AR Path="/5E634176/5E802260" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E802260" Ref="R?"  Part="1" 
AR Path="/5E802260" Ref="R20"  Part="1" 
F 0 "R20" V 3895 1800 50  0000 C CNN
F 1 "120 Ohm" V 3986 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 1800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 1800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 1800 50  0001 C CNN "Description"
F 5 "C149652" H 4100 1800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 1800 50  0001 C CNN "MPN"
	1    4100 1800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E80226A
P 4700 1700
AR Path="/5E63399D/5E80226A" Ref="A?"  Part="1" 
AR Path="/5E634176/5E80226A" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E80226A" Ref="A?"  Part="1" 
AR Path="/5E80226A" Ref="A20"  Part="1" 
F 0 "A20" H 4800 1749 50  0000 L CNN
F 1 "Solder_Pad" H 4800 1658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 1700 50  0001 C CNN
F 3 "~" H 4700 1700 50  0001 C CNN
	1    4700 1700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802274
P 4350 2300
AR Path="/5E63399D/5E802274" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802274" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802274" Ref="SW?"  Part="1" 
AR Path="/5E802274" Ref="SW20"  Part="1" 
F 0 "SW20" H 4350 2585 50  0000 C CNN
F 1 "SW_Push" H 4350 2494 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 2500 50  0001 C CNN
F 3 "~" H 4350 2500 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 2300 50  0001 C CNN "MPN"
	1    4350 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1800 4550 1800
Wire Wire Line
	4250 1800 4200 1800
Wire Wire Line
	4000 1800 3900 1800
Wire Wire Line
	3900 1800 3900 1900
Wire Wire Line
	3950 2300 4150 2300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E802283
P 4700 2200
AR Path="/5E63399D/5E802283" Ref="B?"  Part="1" 
AR Path="/5E634176/5E802283" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E802283" Ref="B?"  Part="1" 
AR Path="/5E802283" Ref="B20"  Part="1" 
F 0 "B20" H 4800 2249 50  0000 L CNN
F 1 "Solder_Pad" H 4800 2158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 2200 50  0001 C CNN
F 3 "~" H 4700 2200 50  0001 C CNN
	1    4700 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2300 4700 2300
$Comp
L dk_LED:LG_R971-KN-1 D20
U 1 1 5E802297
P 4350 1800
F 0 "D20" H 4300 1553 60  0000 C CNN
F 1 "NCD0805O1" H 4300 1659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 2000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 2100 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 2200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 2300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 2400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 2500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 2600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 2700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 2800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 2900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 3000 60  0001 L CNN "Status"
F 13 "C84262" H 4350 1800 50  0001 C CNN "LCSC"
	1    4350 1800
	-1   0    0    1   
$EndComp
Text Label 3950 2300 2    50   ~ 0
VOUT
Text Label 3900 1900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8022A3
P 4100 2800
AR Path="/5E63399D/5E8022A3" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8022A3" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8022A3" Ref="R?"  Part="1" 
AR Path="/5E8022A3" Ref="R21"  Part="1" 
F 0 "R21" V 3895 2800 50  0000 C CNN
F 1 "120 Ohm" V 3986 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 2800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 2800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 2800 50  0001 C CNN "Description"
F 5 "C149652" H 4100 2800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 2800 50  0001 C CNN "MPN"
	1    4100 2800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8022AD
P 4700 2700
AR Path="/5E63399D/5E8022AD" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8022AD" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8022AD" Ref="A?"  Part="1" 
AR Path="/5E8022AD" Ref="A21"  Part="1" 
F 0 "A21" H 4800 2749 50  0000 L CNN
F 1 "Solder_Pad" H 4800 2658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 2700 50  0001 C CNN
F 3 "~" H 4700 2700 50  0001 C CNN
	1    4700 2700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8022B7
P 4350 3300
AR Path="/5E63399D/5E8022B7" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8022B7" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8022B7" Ref="SW?"  Part="1" 
AR Path="/5E8022B7" Ref="SW21"  Part="1" 
F 0 "SW21" H 4350 3585 50  0000 C CNN
F 1 "SW_Push" H 4350 3494 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 3500 50  0001 C CNN
F 3 "~" H 4350 3500 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 3300 50  0001 C CNN "MPN"
	1    4350 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2800 4550 2800
Wire Wire Line
	4250 2800 4200 2800
Wire Wire Line
	4000 2800 3900 2800
Wire Wire Line
	3900 2800 3900 2900
Wire Wire Line
	3950 3300 4150 3300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8022C6
P 4700 3200
AR Path="/5E63399D/5E8022C6" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8022C6" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8022C6" Ref="B?"  Part="1" 
AR Path="/5E8022C6" Ref="B21"  Part="1" 
F 0 "B21" H 4800 3249 50  0000 L CNN
F 1 "Solder_Pad" H 4800 3158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 3200 50  0001 C CNN
F 3 "~" H 4700 3200 50  0001 C CNN
	1    4700 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3300 4700 3300
$Comp
L dk_LED:LG_R971-KN-1 D21
U 1 1 5E8022DA
P 4350 2800
F 0 "D21" H 4300 2553 60  0000 C CNN
F 1 "NCD0805O1" H 4300 2659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 3000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 3100 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 3200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 3300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 3400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 3500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 3600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 3700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 3800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 3900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 4000 60  0001 L CNN "Status"
F 13 "C84262" H 4350 2800 50  0001 C CNN "LCSC"
	1    4350 2800
	-1   0    0    1   
$EndComp
Text Label 3950 3300 2    50   ~ 0
VOUT
Text Label 3900 2900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8022E6
P 4100 3850
AR Path="/5E63399D/5E8022E6" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8022E6" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8022E6" Ref="R?"  Part="1" 
AR Path="/5E8022E6" Ref="R22"  Part="1" 
F 0 "R22" V 3895 3850 50  0000 C CNN
F 1 "120 Ohm" V 3986 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 3850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 3850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 3850 50  0001 C CNN "Description"
F 5 "C149652" H 4100 3850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 3850 50  0001 C CNN "MPN"
	1    4100 3850
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8022F0
P 4700 3750
AR Path="/5E63399D/5E8022F0" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8022F0" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8022F0" Ref="A?"  Part="1" 
AR Path="/5E8022F0" Ref="A22"  Part="1" 
F 0 "A22" H 4800 3799 50  0000 L CNN
F 1 "Solder_Pad" H 4800 3708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 3750 50  0001 C CNN
F 3 "~" H 4700 3750 50  0001 C CNN
	1    4700 3750
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8022FA
P 4350 4350
AR Path="/5E63399D/5E8022FA" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8022FA" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8022FA" Ref="SW?"  Part="1" 
AR Path="/5E8022FA" Ref="SW22"  Part="1" 
F 0 "SW22" H 4350 4635 50  0000 C CNN
F 1 "SW_Push" H 4350 4544 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 4550 50  0001 C CNN
F 3 "~" H 4350 4550 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 4350 50  0001 C CNN "MPN"
	1    4350 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3850 4550 3850
Wire Wire Line
	4250 3850 4200 3850
Wire Wire Line
	4000 3850 3900 3850
Wire Wire Line
	3900 3850 3900 3950
Wire Wire Line
	3950 4350 4150 4350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E802309
P 4700 4250
AR Path="/5E63399D/5E802309" Ref="B?"  Part="1" 
AR Path="/5E634176/5E802309" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E802309" Ref="B?"  Part="1" 
AR Path="/5E802309" Ref="B22"  Part="1" 
F 0 "B22" H 4800 4299 50  0000 L CNN
F 1 "Solder_Pad" H 4800 4208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 4250 50  0001 C CNN
F 3 "~" H 4700 4250 50  0001 C CNN
	1    4700 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4350 4700 4350
$Comp
L dk_LED:LG_R971-KN-1 D22
U 1 1 5E80231D
P 4350 3850
F 0 "D22" H 4300 3603 60  0000 C CNN
F 1 "NCD0805O1" H 4300 3709 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 4050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 4150 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 4350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 4450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 4550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 4850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 5050 60  0001 L CNN "Status"
F 13 "C84262" H 4350 3850 50  0001 C CNN "LCSC"
	1    4350 3850
	-1   0    0    1   
$EndComp
Text Label 3950 4350 2    50   ~ 0
VOUT
Text Label 3900 3950 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E802329
P 4100 4900
AR Path="/5E63399D/5E802329" Ref="R?"  Part="1" 
AR Path="/5E634176/5E802329" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E802329" Ref="R?"  Part="1" 
AR Path="/5E802329" Ref="R23"  Part="1" 
F 0 "R23" V 3895 4900 50  0000 C CNN
F 1 "120 Ohm" V 3986 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 4900 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 4900 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 4900 50  0001 C CNN "Description"
F 5 "C149652" H 4100 4900 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 4900 50  0001 C CNN "MPN"
	1    4100 4900
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E802333
P 4700 4800
AR Path="/5E63399D/5E802333" Ref="A?"  Part="1" 
AR Path="/5E634176/5E802333" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E802333" Ref="A?"  Part="1" 
AR Path="/5E802333" Ref="A23"  Part="1" 
F 0 "A23" H 4800 4849 50  0000 L CNN
F 1 "Solder_Pad" H 4800 4758 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 4800 50  0001 C CNN
F 3 "~" H 4700 4800 50  0001 C CNN
	1    4700 4800
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E80233D
P 4350 5400
AR Path="/5E63399D/5E80233D" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E80233D" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E80233D" Ref="SW?"  Part="1" 
AR Path="/5E80233D" Ref="SW23"  Part="1" 
F 0 "SW23" H 4350 5685 50  0000 C CNN
F 1 "SW_Push" H 4350 5594 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 5600 50  0001 C CNN
F 3 "~" H 4350 5600 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 5400 50  0001 C CNN "MPN"
	1    4350 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4900 4550 4900
Wire Wire Line
	4250 4900 4200 4900
Wire Wire Line
	4000 4900 3900 4900
Wire Wire Line
	3900 4900 3900 5000
Wire Wire Line
	3950 5400 4150 5400
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E80234C
P 4700 5300
AR Path="/5E63399D/5E80234C" Ref="B?"  Part="1" 
AR Path="/5E634176/5E80234C" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E80234C" Ref="B?"  Part="1" 
AR Path="/5E80234C" Ref="B23"  Part="1" 
F 0 "B23" H 4800 5349 50  0000 L CNN
F 1 "Solder_Pad" H 4800 5258 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 5300 50  0001 C CNN
F 3 "~" H 4700 5300 50  0001 C CNN
	1    4700 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 5400 4700 5400
$Comp
L dk_LED:LG_R971-KN-1 D23
U 1 1 5E802360
P 4350 4900
F 0 "D23" H 4300 4653 60  0000 C CNN
F 1 "NCD0805O1" H 4300 4759 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 5100 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 5200 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 5300 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 5400 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 5500 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 5600 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 5700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 5800 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 5900 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 6000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 6100 60  0001 L CNN "Status"
F 13 "C84262" H 4350 4900 50  0001 C CNN "LCSC"
	1    4350 4900
	-1   0    0    1   
$EndComp
Text Label 3950 5400 2    50   ~ 0
VOUT
Text Label 3900 5000 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E80236C
P 4100 5950
AR Path="/5E63399D/5E80236C" Ref="R?"  Part="1" 
AR Path="/5E634176/5E80236C" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E80236C" Ref="R?"  Part="1" 
AR Path="/5E80236C" Ref="R24"  Part="1" 
F 0 "R24" V 3895 5950 50  0000 C CNN
F 1 "120 Ohm" V 3986 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 5950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 5950 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 5950 50  0001 C CNN "Description"
F 5 "C149652" H 4100 5950 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 5950 50  0001 C CNN "MPN"
	1    4100 5950
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E802376
P 4700 5850
AR Path="/5E63399D/5E802376" Ref="A?"  Part="1" 
AR Path="/5E634176/5E802376" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E802376" Ref="A?"  Part="1" 
AR Path="/5E802376" Ref="A24"  Part="1" 
F 0 "A24" H 4800 5899 50  0000 L CNN
F 1 "Solder_Pad" H 4800 5808 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 5850 50  0001 C CNN
F 3 "~" H 4700 5850 50  0001 C CNN
	1    4700 5850
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802380
P 4350 6450
AR Path="/5E63399D/5E802380" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802380" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802380" Ref="SW?"  Part="1" 
AR Path="/5E802380" Ref="SW24"  Part="1" 
F 0 "SW24" H 4350 6735 50  0000 C CNN
F 1 "SW_Push" H 4350 6644 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 6650 50  0001 C CNN
F 3 "~" H 4350 6650 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 6450 50  0001 C CNN "MPN"
	1    4350 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 5950 4550 5950
Wire Wire Line
	4250 5950 4200 5950
Wire Wire Line
	4000 5950 3900 5950
Wire Wire Line
	3900 5950 3900 6050
Wire Wire Line
	3950 6450 4150 6450
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E80238F
P 4700 6350
AR Path="/5E63399D/5E80238F" Ref="B?"  Part="1" 
AR Path="/5E634176/5E80238F" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E80238F" Ref="B?"  Part="1" 
AR Path="/5E80238F" Ref="B24"  Part="1" 
F 0 "B24" H 4800 6399 50  0000 L CNN
F 1 "Solder_Pad" H 4800 6308 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 6350 50  0001 C CNN
F 3 "~" H 4700 6350 50  0001 C CNN
	1    4700 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 6450 4700 6450
$Comp
L dk_LED:LG_R971-KN-1 D24
U 1 1 5E8023A3
P 4350 5950
F 0 "D24" H 4300 5703 60  0000 C CNN
F 1 "NCD0805O1" H 4300 5809 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 6150 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 6250 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 6350 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 6450 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 6550 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 6650 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 6750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 6850 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 6950 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 7050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 7150 60  0001 L CNN "Status"
F 13 "C84262" H 4350 5950 50  0001 C CNN "LCSC"
	1    4350 5950
	-1   0    0    1   
$EndComp
Text Label 3950 6450 2    50   ~ 0
VOUT
Text Label 3900 6050 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8023AF
P 4100 7000
AR Path="/5E63399D/5E8023AF" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8023AF" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8023AF" Ref="R?"  Part="1" 
AR Path="/5E8023AF" Ref="R25"  Part="1" 
F 0 "R25" V 3895 7000 50  0000 C CNN
F 1 "120 Ohm" V 3986 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 7000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 7000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 7000 50  0001 C CNN "Description"
F 5 "C149652" H 4100 7000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 7000 50  0001 C CNN "MPN"
	1    4100 7000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8023B9
P 4700 6900
AR Path="/5E63399D/5E8023B9" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8023B9" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8023B9" Ref="A?"  Part="1" 
AR Path="/5E8023B9" Ref="A25"  Part="1" 
F 0 "A25" H 4800 6949 50  0000 L CNN
F 1 "Solder_Pad" H 4800 6858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 6900 50  0001 C CNN
F 3 "~" H 4700 6900 50  0001 C CNN
	1    4700 6900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8023C3
P 4350 7500
AR Path="/5E63399D/5E8023C3" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8023C3" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8023C3" Ref="SW?"  Part="1" 
AR Path="/5E8023C3" Ref="SW25"  Part="1" 
F 0 "SW25" H 4350 7785 50  0000 C CNN
F 1 "SW_Push" H 4350 7694 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 7700 50  0001 C CNN
F 3 "~" H 4350 7700 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 7500 50  0001 C CNN "MPN"
	1    4350 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 7000 4550 7000
Wire Wire Line
	4250 7000 4200 7000
Wire Wire Line
	4000 7000 3900 7000
Wire Wire Line
	3900 7000 3900 7100
Wire Wire Line
	3950 7500 4150 7500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8023D2
P 4700 7400
AR Path="/5E63399D/5E8023D2" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8023D2" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8023D2" Ref="B?"  Part="1" 
AR Path="/5E8023D2" Ref="B25"  Part="1" 
F 0 "B25" H 4800 7449 50  0000 L CNN
F 1 "Solder_Pad" H 4800 7358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 7400 50  0001 C CNN
F 3 "~" H 4700 7400 50  0001 C CNN
	1    4700 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 7500 4700 7500
$Comp
L dk_LED:LG_R971-KN-1 D25
U 1 1 5E8023E6
P 4350 7000
F 0 "D25" H 4300 6753 60  0000 C CNN
F 1 "NCD0805O1" H 4300 6859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 7200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 7300 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 7400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 7500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 7600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 7700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 7800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 7900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 8000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 8100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 8200 60  0001 L CNN "Status"
F 13 "C84262" H 4350 7000 50  0001 C CNN "LCSC"
	1    4350 7000
	-1   0    0    1   
$EndComp
Text Label 3950 7500 2    50   ~ 0
VOUT
Text Label 3900 7100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8023F2
P 4100 8000
AR Path="/5E63399D/5E8023F2" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8023F2" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8023F2" Ref="R?"  Part="1" 
AR Path="/5E8023F2" Ref="R26"  Part="1" 
F 0 "R26" V 3895 8000 50  0000 C CNN
F 1 "120 Ohm" V 3986 8000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 8000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 8000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 8000 50  0001 C CNN "Description"
F 5 "C149652" H 4100 8000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 8000 50  0001 C CNN "MPN"
	1    4100 8000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8023FC
P 4700 7900
AR Path="/5E63399D/5E8023FC" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8023FC" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8023FC" Ref="A?"  Part="1" 
AR Path="/5E8023FC" Ref="A26"  Part="1" 
F 0 "A26" H 4800 7949 50  0000 L CNN
F 1 "Solder_Pad" H 4800 7858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 7900 50  0001 C CNN
F 3 "~" H 4700 7900 50  0001 C CNN
	1    4700 7900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802406
P 4350 8500
AR Path="/5E63399D/5E802406" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802406" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802406" Ref="SW?"  Part="1" 
AR Path="/5E802406" Ref="SW26"  Part="1" 
F 0 "SW26" H 4350 8785 50  0000 C CNN
F 1 "SW_Push" H 4350 8694 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 8700 50  0001 C CNN
F 3 "~" H 4350 8700 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 8500 50  0001 C CNN "MPN"
	1    4350 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 8000 4550 8000
Wire Wire Line
	4250 8000 4200 8000
Wire Wire Line
	4000 8000 3900 8000
Wire Wire Line
	3900 8000 3900 8100
Wire Wire Line
	3950 8500 4150 8500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E802415
P 4700 8400
AR Path="/5E63399D/5E802415" Ref="B?"  Part="1" 
AR Path="/5E634176/5E802415" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E802415" Ref="B?"  Part="1" 
AR Path="/5E802415" Ref="B26"  Part="1" 
F 0 "B26" H 4800 8449 50  0000 L CNN
F 1 "Solder_Pad" H 4800 8358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 8400 50  0001 C CNN
F 3 "~" H 4700 8400 50  0001 C CNN
	1    4700 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 8500 4700 8500
$Comp
L dk_LED:LG_R971-KN-1 D26
U 1 1 5E802429
P 4350 8000
F 0 "D26" H 4300 7753 60  0000 C CNN
F 1 "NCD0805O1" H 4300 7859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 8200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 8300 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 8400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 8500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 8600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 8700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 8800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 8900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 9000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 9100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 9200 60  0001 L CNN "Status"
F 13 "C84262" H 4350 8000 50  0001 C CNN "LCSC"
	1    4350 8000
	-1   0    0    1   
$EndComp
Text Label 3950 8500 2    50   ~ 0
VOUT
Text Label 3900 8100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E802435
P 4100 9050
AR Path="/5E63399D/5E802435" Ref="R?"  Part="1" 
AR Path="/5E634176/5E802435" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E802435" Ref="R?"  Part="1" 
AR Path="/5E802435" Ref="R27"  Part="1" 
F 0 "R27" V 3895 9050 50  0000 C CNN
F 1 "120 Ohm" V 3986 9050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4100 9050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 4100 9050 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 4100 9050 50  0001 C CNN "Description"
F 5 "C149652" H 4100 9050 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 4100 9050 50  0001 C CNN "MPN"
	1    4100 9050
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E80243F
P 4700 8950
AR Path="/5E63399D/5E80243F" Ref="A?"  Part="1" 
AR Path="/5E634176/5E80243F" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E80243F" Ref="A?"  Part="1" 
AR Path="/5E80243F" Ref="A27"  Part="1" 
F 0 "A27" H 4800 8999 50  0000 L CNN
F 1 "Solder_Pad" H 4800 8908 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 8950 50  0001 C CNN
F 3 "~" H 4700 8950 50  0001 C CNN
	1    4700 8950
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802449
P 4350 9550
AR Path="/5E63399D/5E802449" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802449" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802449" Ref="SW?"  Part="1" 
AR Path="/5E802449" Ref="SW27"  Part="1" 
F 0 "SW27" H 4350 9835 50  0000 C CNN
F 1 "SW_Push" H 4350 9744 50  0000 C CNN
F 2 "footprints:TL1105" H 4350 9750 50  0001 C CNN
F 3 "~" H 4350 9750 50  0001 C CNN
F 4 "TL1105LF250Q" H 4350 9550 50  0001 C CNN "MPN"
	1    4350 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 9050 4550 9050
Wire Wire Line
	4250 9050 4200 9050
Wire Wire Line
	4000 9050 3900 9050
Wire Wire Line
	3900 9050 3900 9150
Wire Wire Line
	3950 9550 4150 9550
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E802458
P 4700 9450
AR Path="/5E63399D/5E802458" Ref="B?"  Part="1" 
AR Path="/5E634176/5E802458" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E802458" Ref="B?"  Part="1" 
AR Path="/5E802458" Ref="B27"  Part="1" 
F 0 "B27" H 4800 9499 50  0000 L CNN
F 1 "Solder_Pad" H 4800 9408 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 4700 9450 50  0001 C CNN
F 3 "~" H 4700 9450 50  0001 C CNN
	1    4700 9450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 9550 4700 9550
$Comp
L dk_LED:LG_R971-KN-1 D27
U 1 1 5E80246C
P 4350 9050
F 0 "D27" H 4300 8803 60  0000 C CNN
F 1 "NCD0805O1" H 4300 8909 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4550 9250 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 4550 9350 60  0001 L CNN
F 4 "475-1410-1-ND" H 4550 9450 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 4550 9550 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 4550 9650 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 4550 9750 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 4550 9850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 4550 9950 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 4550 10050 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 4550 10150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4550 10250 60  0001 L CNN "Status"
F 13 "C84262" H 4350 9050 50  0001 C CNN "LCSC"
	1    4350 9050
	-1   0    0    1   
$EndComp
Text Label 3950 9550 2    50   ~ 0
VOUT
Text Label 3900 9150 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E802478
P 5700 800
AR Path="/5E63399D/5E802478" Ref="R?"  Part="1" 
AR Path="/5E634176/5E802478" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E802478" Ref="R?"  Part="1" 
AR Path="/5E802478" Ref="R28"  Part="1" 
F 0 "R28" V 5495 800 50  0000 C CNN
F 1 "120 Ohm" V 5586 800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 800 50  0001 C CNN "Description"
F 5 "C149652" H 5700 800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 800 50  0001 C CNN "MPN"
	1    5700 800 
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E802482
P 6300 700
AR Path="/5E63399D/5E802482" Ref="A?"  Part="1" 
AR Path="/5E634176/5E802482" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E802482" Ref="A?"  Part="1" 
AR Path="/5E802482" Ref="A28"  Part="1" 
F 0 "A28" H 6400 749 50  0000 L CNN
F 1 "Solder_Pad" H 6400 658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 700 50  0001 C CNN
F 3 "~" H 6300 700 50  0001 C CNN
	1    6300 700 
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E80248C
P 5950 1300
AR Path="/5E63399D/5E80248C" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E80248C" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E80248C" Ref="SW?"  Part="1" 
AR Path="/5E80248C" Ref="SW28"  Part="1" 
F 0 "SW28" H 5950 1585 50  0000 C CNN
F 1 "SW_Push" H 5950 1494 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 1500 50  0001 C CNN
F 3 "~" H 5950 1500 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 1300 50  0001 C CNN "MPN"
	1    5950 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 800  6150 800 
Wire Wire Line
	5850 800  5800 800 
Wire Wire Line
	5600 800  5500 800 
Wire Wire Line
	5500 800  5500 900 
Wire Wire Line
	5550 1300 5750 1300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E80249B
P 6300 1200
AR Path="/5E63399D/5E80249B" Ref="B?"  Part="1" 
AR Path="/5E634176/5E80249B" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E80249B" Ref="B?"  Part="1" 
AR Path="/5E80249B" Ref="B28"  Part="1" 
F 0 "B28" H 6400 1249 50  0000 L CNN
F 1 "Solder_Pad" H 6400 1158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 1200 50  0001 C CNN
F 3 "~" H 6300 1200 50  0001 C CNN
	1    6300 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1300 6300 1300
$Comp
L dk_LED:LG_R971-KN-1 D28
U 1 1 5E8024AF
P 5950 800
F 0 "D28" H 5900 553 60  0000 C CNN
F 1 "NCD0805O1" H 5900 659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 1000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 1100 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 1200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 1300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 1400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 1500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 1600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 1700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 1800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 1900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 2000 60  0001 L CNN "Status"
F 13 "C84262" H 5950 800 50  0001 C CNN "LCSC"
	1    5950 800 
	-1   0    0    1   
$EndComp
Text Label 5550 1300 2    50   ~ 0
VOUT
Text Label 5500 900  2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8024BB
P 5700 1750
AR Path="/5E63399D/5E8024BB" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8024BB" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8024BB" Ref="R?"  Part="1" 
AR Path="/5E8024BB" Ref="R29"  Part="1" 
F 0 "R29" V 5495 1750 50  0000 C CNN
F 1 "120 Ohm" V 5586 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 1750 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 1750 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 1750 50  0001 C CNN "Description"
F 5 "C149652" H 5700 1750 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 1750 50  0001 C CNN "MPN"
	1    5700 1750
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8024C5
P 6300 1650
AR Path="/5E63399D/5E8024C5" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8024C5" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8024C5" Ref="A?"  Part="1" 
AR Path="/5E8024C5" Ref="A29"  Part="1" 
F 0 "A29" H 6400 1699 50  0000 L CNN
F 1 "Solder_Pad" H 6400 1608 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 1650 50  0001 C CNN
F 3 "~" H 6300 1650 50  0001 C CNN
	1    6300 1650
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8024CF
P 5950 2250
AR Path="/5E63399D/5E8024CF" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8024CF" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8024CF" Ref="SW?"  Part="1" 
AR Path="/5E8024CF" Ref="SW29"  Part="1" 
F 0 "SW29" H 5950 2535 50  0000 C CNN
F 1 "SW_Push" H 5950 2444 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 2450 50  0001 C CNN
F 3 "~" H 5950 2450 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 2250 50  0001 C CNN "MPN"
	1    5950 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 1750 6150 1750
Wire Wire Line
	5850 1750 5800 1750
Wire Wire Line
	5600 1750 5500 1750
Wire Wire Line
	5500 1750 5500 1850
Wire Wire Line
	5550 2250 5750 2250
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8024DE
P 6300 2150
AR Path="/5E63399D/5E8024DE" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8024DE" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8024DE" Ref="B?"  Part="1" 
AR Path="/5E8024DE" Ref="B29"  Part="1" 
F 0 "B29" H 6400 2199 50  0000 L CNN
F 1 "Solder_Pad" H 6400 2108 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 2150 50  0001 C CNN
F 3 "~" H 6300 2150 50  0001 C CNN
	1    6300 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2250 6300 2250
$Comp
L dk_LED:LG_R971-KN-1 D29
U 1 1 5E8024F2
P 5950 1750
F 0 "D29" H 5900 1503 60  0000 C CNN
F 1 "NCD0805O1" H 5900 1609 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 1950 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 2050 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 2150 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 2250 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 2350 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 2450 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 2550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 2650 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 2750 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 2850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 2950 60  0001 L CNN "Status"
F 13 "C84262" H 5950 1750 50  0001 C CNN "LCSC"
	1    5950 1750
	-1   0    0    1   
$EndComp
Text Label 5550 2250 2    50   ~ 0
VOUT
Text Label 5500 1850 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8024FE
P 5700 2800
AR Path="/5E63399D/5E8024FE" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8024FE" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8024FE" Ref="R?"  Part="1" 
AR Path="/5E8024FE" Ref="R30"  Part="1" 
F 0 "R30" V 5495 2800 50  0000 C CNN
F 1 "120 Ohm" V 5586 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 2800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 2800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 2800 50  0001 C CNN "Description"
F 5 "C149652" H 5700 2800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 2800 50  0001 C CNN "MPN"
	1    5700 2800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E802508
P 6300 2700
AR Path="/5E63399D/5E802508" Ref="A?"  Part="1" 
AR Path="/5E634176/5E802508" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E802508" Ref="A?"  Part="1" 
AR Path="/5E802508" Ref="A30"  Part="1" 
F 0 "A30" H 6400 2749 50  0000 L CNN
F 1 "Solder_Pad" H 6400 2658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 2700 50  0001 C CNN
F 3 "~" H 6300 2700 50  0001 C CNN
	1    6300 2700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802512
P 5950 3300
AR Path="/5E63399D/5E802512" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802512" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802512" Ref="SW?"  Part="1" 
AR Path="/5E802512" Ref="SW30"  Part="1" 
F 0 "SW30" H 5950 3585 50  0000 C CNN
F 1 "SW_Push" H 5950 3494 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 3500 50  0001 C CNN
F 3 "~" H 5950 3500 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 3300 50  0001 C CNN "MPN"
	1    5950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 2800 6150 2800
Wire Wire Line
	5850 2800 5800 2800
Wire Wire Line
	5600 2800 5500 2800
Wire Wire Line
	5500 2800 5500 2900
Wire Wire Line
	5550 3300 5750 3300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E802521
P 6300 3200
AR Path="/5E63399D/5E802521" Ref="B?"  Part="1" 
AR Path="/5E634176/5E802521" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E802521" Ref="B?"  Part="1" 
AR Path="/5E802521" Ref="B30"  Part="1" 
F 0 "B30" H 6400 3249 50  0000 L CNN
F 1 "Solder_Pad" H 6400 3158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 3200 50  0001 C CNN
F 3 "~" H 6300 3200 50  0001 C CNN
	1    6300 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3300 6300 3300
$Comp
L dk_LED:LG_R971-KN-1 D30
U 1 1 5E802535
P 5950 2800
F 0 "D30" H 5900 2553 60  0000 C CNN
F 1 "NCD0805O1" H 5900 2659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 3000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 3100 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 3200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 3300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 3400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 3500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 3600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 3700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 3800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 3900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 4000 60  0001 L CNN "Status"
F 13 "C84262" H 5950 2800 50  0001 C CNN "LCSC"
	1    5950 2800
	-1   0    0    1   
$EndComp
Text Label 5550 3300 2    50   ~ 0
VOUT
Text Label 5500 2900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E802541
P 5700 3850
AR Path="/5E63399D/5E802541" Ref="R?"  Part="1" 
AR Path="/5E634176/5E802541" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E802541" Ref="R?"  Part="1" 
AR Path="/5E802541" Ref="R31"  Part="1" 
F 0 "R31" V 5495 3850 50  0000 C CNN
F 1 "120 Ohm" V 5586 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 3850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 3850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 3850 50  0001 C CNN "Description"
F 5 "C149652" H 5700 3850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 3850 50  0001 C CNN "MPN"
	1    5700 3850
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E80254B
P 6300 3750
AR Path="/5E63399D/5E80254B" Ref="A?"  Part="1" 
AR Path="/5E634176/5E80254B" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E80254B" Ref="A?"  Part="1" 
AR Path="/5E80254B" Ref="A31"  Part="1" 
F 0 "A31" H 6400 3799 50  0000 L CNN
F 1 "Solder_Pad" H 6400 3708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 3750 50  0001 C CNN
F 3 "~" H 6300 3750 50  0001 C CNN
	1    6300 3750
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802555
P 5950 4350
AR Path="/5E63399D/5E802555" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802555" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802555" Ref="SW?"  Part="1" 
AR Path="/5E802555" Ref="SW31"  Part="1" 
F 0 "SW31" H 5950 4635 50  0000 C CNN
F 1 "SW_Push" H 5950 4544 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 4550 50  0001 C CNN
F 3 "~" H 5950 4550 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 4350 50  0001 C CNN "MPN"
	1    5950 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3850 6150 3850
Wire Wire Line
	5850 3850 5800 3850
Wire Wire Line
	5600 3850 5500 3850
Wire Wire Line
	5500 3850 5500 3950
Wire Wire Line
	5550 4350 5750 4350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E802564
P 6300 4250
AR Path="/5E63399D/5E802564" Ref="B?"  Part="1" 
AR Path="/5E634176/5E802564" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E802564" Ref="B?"  Part="1" 
AR Path="/5E802564" Ref="B31"  Part="1" 
F 0 "B31" H 6400 4299 50  0000 L CNN
F 1 "Solder_Pad" H 6400 4208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 4250 50  0001 C CNN
F 3 "~" H 6300 4250 50  0001 C CNN
	1    6300 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4350 6300 4350
$Comp
L dk_LED:LG_R971-KN-1 D31
U 1 1 5E802578
P 5950 3850
F 0 "D31" H 5900 3603 60  0000 C CNN
F 1 "NCD0805O1" H 5900 3709 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 4050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 4150 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 4350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 4450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 4550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 4850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 5050 60  0001 L CNN "Status"
F 13 "C84262" H 5950 3850 50  0001 C CNN "LCSC"
	1    5950 3850
	-1   0    0    1   
$EndComp
Text Label 5550 4350 2    50   ~ 0
VOUT
Text Label 5500 3950 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E802584
P 5700 4900
AR Path="/5E63399D/5E802584" Ref="R?"  Part="1" 
AR Path="/5E634176/5E802584" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E802584" Ref="R?"  Part="1" 
AR Path="/5E802584" Ref="R32"  Part="1" 
F 0 "R32" V 5495 4900 50  0000 C CNN
F 1 "120 Ohm" V 5586 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 4900 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 4900 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 4900 50  0001 C CNN "Description"
F 5 "C149652" H 5700 4900 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 4900 50  0001 C CNN "MPN"
	1    5700 4900
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E80258E
P 6300 4800
AR Path="/5E63399D/5E80258E" Ref="A?"  Part="1" 
AR Path="/5E634176/5E80258E" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E80258E" Ref="A?"  Part="1" 
AR Path="/5E80258E" Ref="A32"  Part="1" 
F 0 "A32" H 6400 4849 50  0000 L CNN
F 1 "Solder_Pad" H 6400 4758 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 4800 50  0001 C CNN
F 3 "~" H 6300 4800 50  0001 C CNN
	1    6300 4800
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802598
P 5950 5400
AR Path="/5E63399D/5E802598" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802598" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802598" Ref="SW?"  Part="1" 
AR Path="/5E802598" Ref="SW32"  Part="1" 
F 0 "SW32" H 5950 5685 50  0000 C CNN
F 1 "SW_Push" H 5950 5594 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 5600 50  0001 C CNN
F 3 "~" H 5950 5600 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 5400 50  0001 C CNN "MPN"
	1    5950 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 4900 6150 4900
Wire Wire Line
	5850 4900 5800 4900
Wire Wire Line
	5600 4900 5500 4900
Wire Wire Line
	5500 4900 5500 5000
Wire Wire Line
	5550 5400 5750 5400
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8025A7
P 6300 5300
AR Path="/5E63399D/5E8025A7" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8025A7" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8025A7" Ref="B?"  Part="1" 
AR Path="/5E8025A7" Ref="B32"  Part="1" 
F 0 "B32" H 6400 5349 50  0000 L CNN
F 1 "Solder_Pad" H 6400 5258 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 5300 50  0001 C CNN
F 3 "~" H 6300 5300 50  0001 C CNN
	1    6300 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 5400 6300 5400
$Comp
L dk_LED:LG_R971-KN-1 D32
U 1 1 5E8025BB
P 5950 4900
F 0 "D32" H 5900 4653 60  0000 C CNN
F 1 "NCD0805O1" H 5900 4759 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 5100 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 5200 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 5300 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 5400 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 5500 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 5600 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 5700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 5800 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 5900 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 6000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 6100 60  0001 L CNN "Status"
F 13 "C84262" H 5950 4900 50  0001 C CNN "LCSC"
	1    5950 4900
	-1   0    0    1   
$EndComp
Text Label 5550 5400 2    50   ~ 0
VOUT
Text Label 5500 5000 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8025C7
P 5700 5950
AR Path="/5E63399D/5E8025C7" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8025C7" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8025C7" Ref="R?"  Part="1" 
AR Path="/5E8025C7" Ref="R33"  Part="1" 
F 0 "R33" V 5495 5950 50  0000 C CNN
F 1 "120 Ohm" V 5586 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 5950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 5950 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 5950 50  0001 C CNN "Description"
F 5 "C149652" H 5700 5950 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 5950 50  0001 C CNN "MPN"
	1    5700 5950
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8025D1
P 6300 5850
AR Path="/5E63399D/5E8025D1" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8025D1" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8025D1" Ref="A?"  Part="1" 
AR Path="/5E8025D1" Ref="A33"  Part="1" 
F 0 "A33" H 6400 5899 50  0000 L CNN
F 1 "Solder_Pad" H 6400 5808 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 5850 50  0001 C CNN
F 3 "~" H 6300 5850 50  0001 C CNN
	1    6300 5850
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8025DB
P 5950 6450
AR Path="/5E63399D/5E8025DB" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8025DB" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8025DB" Ref="SW?"  Part="1" 
AR Path="/5E8025DB" Ref="SW33"  Part="1" 
F 0 "SW33" H 5950 6735 50  0000 C CNN
F 1 "SW_Push" H 5950 6644 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 6650 50  0001 C CNN
F 3 "~" H 5950 6650 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 6450 50  0001 C CNN "MPN"
	1    5950 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 5950 6150 5950
Wire Wire Line
	5850 5950 5800 5950
Wire Wire Line
	5600 5950 5500 5950
Wire Wire Line
	5500 5950 5500 6050
Wire Wire Line
	5550 6450 5750 6450
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8025EA
P 6300 6350
AR Path="/5E63399D/5E8025EA" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8025EA" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8025EA" Ref="B?"  Part="1" 
AR Path="/5E8025EA" Ref="B33"  Part="1" 
F 0 "B33" H 6400 6399 50  0000 L CNN
F 1 "Solder_Pad" H 6400 6308 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 6350 50  0001 C CNN
F 3 "~" H 6300 6350 50  0001 C CNN
	1    6300 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 6450 6300 6450
$Comp
L dk_LED:LG_R971-KN-1 D33
U 1 1 5E8025FE
P 5950 5950
F 0 "D33" H 5900 5703 60  0000 C CNN
F 1 "NCD0805O1" H 5900 5809 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 6150 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 6250 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 6350 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 6450 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 6550 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 6650 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 6750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 6850 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 6950 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 7050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 7150 60  0001 L CNN "Status"
F 13 "C84262" H 5950 5950 50  0001 C CNN "LCSC"
	1    5950 5950
	-1   0    0    1   
$EndComp
Text Label 5550 6450 2    50   ~ 0
VOUT
Text Label 5500 6050 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E80260A
P 5700 7000
AR Path="/5E63399D/5E80260A" Ref="R?"  Part="1" 
AR Path="/5E634176/5E80260A" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E80260A" Ref="R?"  Part="1" 
AR Path="/5E80260A" Ref="R34"  Part="1" 
F 0 "R34" V 5495 7000 50  0000 C CNN
F 1 "120 Ohm" V 5586 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 7000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 7000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 7000 50  0001 C CNN "Description"
F 5 "C149652" H 5700 7000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 7000 50  0001 C CNN "MPN"
	1    5700 7000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E802614
P 6300 6900
AR Path="/5E63399D/5E802614" Ref="A?"  Part="1" 
AR Path="/5E634176/5E802614" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E802614" Ref="A?"  Part="1" 
AR Path="/5E802614" Ref="A34"  Part="1" 
F 0 "A34" H 6400 6949 50  0000 L CNN
F 1 "Solder_Pad" H 6400 6858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 6900 50  0001 C CNN
F 3 "~" H 6300 6900 50  0001 C CNN
	1    6300 6900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E80261E
P 5950 7500
AR Path="/5E63399D/5E80261E" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E80261E" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E80261E" Ref="SW?"  Part="1" 
AR Path="/5E80261E" Ref="SW34"  Part="1" 
F 0 "SW34" H 5950 7785 50  0000 C CNN
F 1 "SW_Push" H 5950 7694 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 7700 50  0001 C CNN
F 3 "~" H 5950 7700 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 7500 50  0001 C CNN "MPN"
	1    5950 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 7000 6150 7000
Wire Wire Line
	5850 7000 5800 7000
Wire Wire Line
	5600 7000 5500 7000
Wire Wire Line
	5500 7000 5500 7100
Wire Wire Line
	5550 7500 5750 7500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E80262D
P 6300 7400
AR Path="/5E63399D/5E80262D" Ref="B?"  Part="1" 
AR Path="/5E634176/5E80262D" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E80262D" Ref="B?"  Part="1" 
AR Path="/5E80262D" Ref="B34"  Part="1" 
F 0 "B34" H 6400 7449 50  0000 L CNN
F 1 "Solder_Pad" H 6400 7358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 7400 50  0001 C CNN
F 3 "~" H 6300 7400 50  0001 C CNN
	1    6300 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 7500 6300 7500
$Comp
L dk_LED:LG_R971-KN-1 D34
U 1 1 5E802641
P 5950 7000
F 0 "D34" H 5900 6753 60  0000 C CNN
F 1 "NCD0805O1" H 5900 6859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 7200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 7300 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 7400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 7500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 7600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 7700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 7800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 7900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 8000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 8100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 8200 60  0001 L CNN "Status"
F 13 "C84262" H 5950 7000 50  0001 C CNN "LCSC"
	1    5950 7000
	-1   0    0    1   
$EndComp
Text Label 5550 7500 2    50   ~ 0
VOUT
Text Label 5500 7100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E80264D
P 5700 8000
AR Path="/5E63399D/5E80264D" Ref="R?"  Part="1" 
AR Path="/5E634176/5E80264D" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E80264D" Ref="R?"  Part="1" 
AR Path="/5E80264D" Ref="R35"  Part="1" 
F 0 "R35" V 5495 8000 50  0000 C CNN
F 1 "120 Ohm" V 5586 8000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 8000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 8000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 8000 50  0001 C CNN "Description"
F 5 "C149652" H 5700 8000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 8000 50  0001 C CNN "MPN"
	1    5700 8000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E802657
P 6300 7900
AR Path="/5E63399D/5E802657" Ref="A?"  Part="1" 
AR Path="/5E634176/5E802657" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E802657" Ref="A?"  Part="1" 
AR Path="/5E802657" Ref="A35"  Part="1" 
F 0 "A35" H 6400 7949 50  0000 L CNN
F 1 "Solder_Pad" H 6400 7858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 7900 50  0001 C CNN
F 3 "~" H 6300 7900 50  0001 C CNN
	1    6300 7900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E802661
P 5950 8500
AR Path="/5E63399D/5E802661" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E802661" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E802661" Ref="SW?"  Part="1" 
AR Path="/5E802661" Ref="SW35"  Part="1" 
F 0 "SW35" H 5950 8785 50  0000 C CNN
F 1 "SW_Push" H 5950 8694 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 8700 50  0001 C CNN
F 3 "~" H 5950 8700 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 8500 50  0001 C CNN "MPN"
	1    5950 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 8000 6150 8000
Wire Wire Line
	5850 8000 5800 8000
Wire Wire Line
	5600 8000 5500 8000
Wire Wire Line
	5500 8000 5500 8100
Wire Wire Line
	5550 8500 5750 8500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E802670
P 6300 8400
AR Path="/5E63399D/5E802670" Ref="B?"  Part="1" 
AR Path="/5E634176/5E802670" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E802670" Ref="B?"  Part="1" 
AR Path="/5E802670" Ref="B35"  Part="1" 
F 0 "B35" H 6400 8449 50  0000 L CNN
F 1 "Solder_Pad" H 6400 8358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 8400 50  0001 C CNN
F 3 "~" H 6300 8400 50  0001 C CNN
	1    6300 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 8500 6300 8500
$Comp
L dk_LED:LG_R971-KN-1 D35
U 1 1 5E802684
P 5950 8000
F 0 "D35" H 5900 7753 60  0000 C CNN
F 1 "NCD0805O1" H 5900 7859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 8200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 8300 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 8400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 8500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 8600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 8700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 8800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 8900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 9000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 9100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 9200 60  0001 L CNN "Status"
F 13 "C84262" H 5950 8000 50  0001 C CNN "LCSC"
	1    5950 8000
	-1   0    0    1   
$EndComp
Text Label 5550 8500 2    50   ~ 0
VOUT
Text Label 5500 8100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E802690
P 5700 9050
AR Path="/5E63399D/5E802690" Ref="R?"  Part="1" 
AR Path="/5E634176/5E802690" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E802690" Ref="R?"  Part="1" 
AR Path="/5E802690" Ref="R36"  Part="1" 
F 0 "R36" V 5495 9050 50  0000 C CNN
F 1 "120 Ohm" V 5586 9050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5700 9050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 5700 9050 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 5700 9050 50  0001 C CNN "Description"
F 5 "C149652" H 5700 9050 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 5700 9050 50  0001 C CNN "MPN"
	1    5700 9050
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E80269A
P 6300 8950
AR Path="/5E63399D/5E80269A" Ref="A?"  Part="1" 
AR Path="/5E634176/5E80269A" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E80269A" Ref="A?"  Part="1" 
AR Path="/5E80269A" Ref="A36"  Part="1" 
F 0 "A36" H 6400 8999 50  0000 L CNN
F 1 "Solder_Pad" H 6400 8908 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 8950 50  0001 C CNN
F 3 "~" H 6300 8950 50  0001 C CNN
	1    6300 8950
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8026A4
P 5950 9550
AR Path="/5E63399D/5E8026A4" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8026A4" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8026A4" Ref="SW?"  Part="1" 
AR Path="/5E8026A4" Ref="SW36"  Part="1" 
F 0 "SW36" H 5950 9835 50  0000 C CNN
F 1 "SW_Push" H 5950 9744 50  0000 C CNN
F 2 "footprints:TL1105" H 5950 9750 50  0001 C CNN
F 3 "~" H 5950 9750 50  0001 C CNN
F 4 "TL1105LF250Q" H 5950 9550 50  0001 C CNN "MPN"
	1    5950 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 9050 6150 9050
Wire Wire Line
	5850 9050 5800 9050
Wire Wire Line
	5600 9050 5500 9050
Wire Wire Line
	5500 9050 5500 9150
Wire Wire Line
	5550 9550 5750 9550
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8026B3
P 6300 9450
AR Path="/5E63399D/5E8026B3" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8026B3" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8026B3" Ref="B?"  Part="1" 
AR Path="/5E8026B3" Ref="B36"  Part="1" 
F 0 "B36" H 6400 9499 50  0000 L CNN
F 1 "Solder_Pad" H 6400 9408 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 6300 9450 50  0001 C CNN
F 3 "~" H 6300 9450 50  0001 C CNN
	1    6300 9450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 9550 6300 9550
$Comp
L dk_LED:LG_R971-KN-1 D36
U 1 1 5E8026C7
P 5950 9050
F 0 "D36" H 5900 8803 60  0000 C CNN
F 1 "NCD0805O1" H 5900 8909 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 9250 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 6150 9350 60  0001 L CNN
F 4 "475-1410-1-ND" H 6150 9450 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 6150 9550 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 6150 9650 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 6150 9750 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 6150 9850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 6150 9950 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 6150 10050 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 6150 10150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6150 10250 60  0001 L CNN "Status"
F 13 "C84262" H 5950 9050 50  0001 C CNN "LCSC"
	1    5950 9050
	-1   0    0    1   
$EndComp
Text Label 5550 9550 2    50   ~ 0
VOUT
Text Label 5500 9150 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8933E6
P 7300 800
AR Path="/5E63399D/5E8933E6" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8933E6" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8933E6" Ref="R?"  Part="1" 
AR Path="/5E8933E6" Ref="R37"  Part="1" 
F 0 "R37" V 7095 800 50  0000 C CNN
F 1 "120 Ohm" V 7186 800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 800 50  0001 C CNN "Description"
F 5 "C149652" H 7300 800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 800 50  0001 C CNN "MPN"
	1    7300 800 
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8933F0
P 7900 700
AR Path="/5E63399D/5E8933F0" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8933F0" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8933F0" Ref="A?"  Part="1" 
AR Path="/5E8933F0" Ref="A37"  Part="1" 
F 0 "A37" H 8000 749 50  0000 L CNN
F 1 "Solder_Pad" H 8000 658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 700 50  0001 C CNN
F 3 "~" H 7900 700 50  0001 C CNN
	1    7900 700 
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8933FA
P 7550 1300
AR Path="/5E63399D/5E8933FA" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8933FA" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8933FA" Ref="SW?"  Part="1" 
AR Path="/5E8933FA" Ref="SW37"  Part="1" 
F 0 "SW37" H 7550 1585 50  0000 C CNN
F 1 "SW_Push" H 7550 1494 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 1500 50  0001 C CNN
F 3 "~" H 7550 1500 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 1300 50  0001 C CNN "MPN"
	1    7550 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 800  7750 800 
Wire Wire Line
	7450 800  7400 800 
Wire Wire Line
	7200 800  7100 800 
Wire Wire Line
	7100 800  7100 900 
Wire Wire Line
	7150 1300 7350 1300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E893409
P 7900 1200
AR Path="/5E63399D/5E893409" Ref="B?"  Part="1" 
AR Path="/5E634176/5E893409" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E893409" Ref="B?"  Part="1" 
AR Path="/5E893409" Ref="B37"  Part="1" 
F 0 "B37" H 8000 1249 50  0000 L CNN
F 1 "Solder_Pad" H 8000 1158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 1200 50  0001 C CNN
F 3 "~" H 7900 1200 50  0001 C CNN
	1    7900 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1300 7900 1300
$Comp
L dk_LED:LG_R971-KN-1 D37
U 1 1 5E89341D
P 7550 800
F 0 "D37" H 7500 553 60  0000 C CNN
F 1 "NCD0805O1" H 7500 659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 1000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 1100 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 1200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 1300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 1400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 1500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 1600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 1700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 1800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 1900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 2000 60  0001 L CNN "Status"
F 13 "C84262" H 7550 800 50  0001 C CNN "LCSC"
	1    7550 800 
	-1   0    0    1   
$EndComp
Text Label 7150 1300 2    50   ~ 0
VOUT
Text Label 7100 900  2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E893429
P 7300 1750
AR Path="/5E63399D/5E893429" Ref="R?"  Part="1" 
AR Path="/5E634176/5E893429" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E893429" Ref="R?"  Part="1" 
AR Path="/5E893429" Ref="R38"  Part="1" 
F 0 "R38" V 7095 1750 50  0000 C CNN
F 1 "120 Ohm" V 7186 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 1750 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 1750 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 1750 50  0001 C CNN "Description"
F 5 "C149652" H 7300 1750 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 1750 50  0001 C CNN "MPN"
	1    7300 1750
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E893433
P 7900 1650
AR Path="/5E63399D/5E893433" Ref="A?"  Part="1" 
AR Path="/5E634176/5E893433" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E893433" Ref="A?"  Part="1" 
AR Path="/5E893433" Ref="A38"  Part="1" 
F 0 "A38" H 8000 1699 50  0000 L CNN
F 1 "Solder_Pad" H 8000 1608 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 1650 50  0001 C CNN
F 3 "~" H 7900 1650 50  0001 C CNN
	1    7900 1650
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E89343D
P 7550 2250
AR Path="/5E63399D/5E89343D" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E89343D" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E89343D" Ref="SW?"  Part="1" 
AR Path="/5E89343D" Ref="SW38"  Part="1" 
F 0 "SW38" H 7550 2535 50  0000 C CNN
F 1 "SW_Push" H 7550 2444 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 2450 50  0001 C CNN
F 3 "~" H 7550 2450 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 2250 50  0001 C CNN "MPN"
	1    7550 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 1750 7750 1750
Wire Wire Line
	7450 1750 7400 1750
Wire Wire Line
	7200 1750 7100 1750
Wire Wire Line
	7100 1750 7100 1850
Wire Wire Line
	7150 2250 7350 2250
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E89344C
P 7900 2150
AR Path="/5E63399D/5E89344C" Ref="B?"  Part="1" 
AR Path="/5E634176/5E89344C" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E89344C" Ref="B?"  Part="1" 
AR Path="/5E89344C" Ref="B38"  Part="1" 
F 0 "B38" H 8000 2199 50  0000 L CNN
F 1 "Solder_Pad" H 8000 2108 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 2150 50  0001 C CNN
F 3 "~" H 7900 2150 50  0001 C CNN
	1    7900 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2250 7900 2250
$Comp
L dk_LED:LG_R971-KN-1 D38
U 1 1 5E893460
P 7550 1750
F 0 "D38" H 7500 1503 60  0000 C CNN
F 1 "NCD0805O1" H 7500 1609 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 1950 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 2050 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 2150 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 2250 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 2350 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 2450 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 2550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 2650 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 2750 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 2850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 2950 60  0001 L CNN "Status"
F 13 "C84262" H 7550 1750 50  0001 C CNN "LCSC"
	1    7550 1750
	-1   0    0    1   
$EndComp
Text Label 7150 2250 2    50   ~ 0
VOUT
Text Label 7100 1850 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E89346C
P 7300 2800
AR Path="/5E63399D/5E89346C" Ref="R?"  Part="1" 
AR Path="/5E634176/5E89346C" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E89346C" Ref="R?"  Part="1" 
AR Path="/5E89346C" Ref="R39"  Part="1" 
F 0 "R39" V 7095 2800 50  0000 C CNN
F 1 "120 Ohm" V 7186 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 2800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 2800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 2800 50  0001 C CNN "Description"
F 5 "C149652" H 7300 2800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 2800 50  0001 C CNN "MPN"
	1    7300 2800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E893476
P 7900 2700
AR Path="/5E63399D/5E893476" Ref="A?"  Part="1" 
AR Path="/5E634176/5E893476" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E893476" Ref="A?"  Part="1" 
AR Path="/5E893476" Ref="A39"  Part="1" 
F 0 "A39" H 8000 2749 50  0000 L CNN
F 1 "Solder_Pad" H 8000 2658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 2700 50  0001 C CNN
F 3 "~" H 7900 2700 50  0001 C CNN
	1    7900 2700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E893480
P 7550 3300
AR Path="/5E63399D/5E893480" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E893480" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E893480" Ref="SW?"  Part="1" 
AR Path="/5E893480" Ref="SW39"  Part="1" 
F 0 "SW39" H 7550 3585 50  0000 C CNN
F 1 "SW_Push" H 7550 3494 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 3500 50  0001 C CNN
F 3 "~" H 7550 3500 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 3300 50  0001 C CNN "MPN"
	1    7550 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 2800 7750 2800
Wire Wire Line
	7450 2800 7400 2800
Wire Wire Line
	7200 2800 7100 2800
Wire Wire Line
	7100 2800 7100 2900
Wire Wire Line
	7150 3300 7350 3300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E89348F
P 7900 3200
AR Path="/5E63399D/5E89348F" Ref="B?"  Part="1" 
AR Path="/5E634176/5E89348F" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E89348F" Ref="B?"  Part="1" 
AR Path="/5E89348F" Ref="B39"  Part="1" 
F 0 "B39" H 8000 3249 50  0000 L CNN
F 1 "Solder_Pad" H 8000 3158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 3200 50  0001 C CNN
F 3 "~" H 7900 3200 50  0001 C CNN
	1    7900 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3300 7900 3300
$Comp
L dk_LED:LG_R971-KN-1 D39
U 1 1 5E8934A3
P 7550 2800
F 0 "D39" H 7500 2553 60  0000 C CNN
F 1 "NCD0805O1" H 7500 2659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 3000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 3100 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 3200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 3300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 3400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 3500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 3600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 3700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 3800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 3900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 4000 60  0001 L CNN "Status"
F 13 "C84262" H 7550 2800 50  0001 C CNN "LCSC"
	1    7550 2800
	-1   0    0    1   
$EndComp
Text Label 7150 3300 2    50   ~ 0
VOUT
Text Label 7100 2900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8934AF
P 7300 3850
AR Path="/5E63399D/5E8934AF" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8934AF" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8934AF" Ref="R?"  Part="1" 
AR Path="/5E8934AF" Ref="R40"  Part="1" 
F 0 "R40" V 7095 3850 50  0000 C CNN
F 1 "120 Ohm" V 7186 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 3850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 3850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 3850 50  0001 C CNN "Description"
F 5 "C149652" H 7300 3850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 3850 50  0001 C CNN "MPN"
	1    7300 3850
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8934B9
P 7900 3750
AR Path="/5E63399D/5E8934B9" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8934B9" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8934B9" Ref="A?"  Part="1" 
AR Path="/5E8934B9" Ref="A40"  Part="1" 
F 0 "A40" H 8000 3799 50  0000 L CNN
F 1 "Solder_Pad" H 8000 3708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 3750 50  0001 C CNN
F 3 "~" H 7900 3750 50  0001 C CNN
	1    7900 3750
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8934C3
P 7550 4350
AR Path="/5E63399D/5E8934C3" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8934C3" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8934C3" Ref="SW?"  Part="1" 
AR Path="/5E8934C3" Ref="SW40"  Part="1" 
F 0 "SW40" H 7550 4635 50  0000 C CNN
F 1 "SW_Push" H 7550 4544 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 4550 50  0001 C CNN
F 3 "~" H 7550 4550 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 4350 50  0001 C CNN "MPN"
	1    7550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 3850 7750 3850
Wire Wire Line
	7450 3850 7400 3850
Wire Wire Line
	7200 3850 7100 3850
Wire Wire Line
	7100 3850 7100 3950
Wire Wire Line
	7150 4350 7350 4350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8934D2
P 7900 4250
AR Path="/5E63399D/5E8934D2" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8934D2" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8934D2" Ref="B?"  Part="1" 
AR Path="/5E8934D2" Ref="B40"  Part="1" 
F 0 "B40" H 8000 4299 50  0000 L CNN
F 1 "Solder_Pad" H 8000 4208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 4250 50  0001 C CNN
F 3 "~" H 7900 4250 50  0001 C CNN
	1    7900 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4350 7900 4350
$Comp
L dk_LED:LG_R971-KN-1 D40
U 1 1 5E8934E6
P 7550 3850
F 0 "D40" H 7500 3603 60  0000 C CNN
F 1 "NCD0805O1" H 7500 3709 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 4050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 4150 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 4350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 4450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 4550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 4850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 5050 60  0001 L CNN "Status"
F 13 "C84262" H 7550 3850 50  0001 C CNN "LCSC"
	1    7550 3850
	-1   0    0    1   
$EndComp
Text Label 7150 4350 2    50   ~ 0
VOUT
Text Label 7100 3950 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8934F2
P 7300 4900
AR Path="/5E63399D/5E8934F2" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8934F2" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8934F2" Ref="R?"  Part="1" 
AR Path="/5E8934F2" Ref="R41"  Part="1" 
F 0 "R41" V 7095 4900 50  0000 C CNN
F 1 "120 Ohm" V 7186 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 4900 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 4900 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 4900 50  0001 C CNN "Description"
F 5 "C149652" H 7300 4900 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 4900 50  0001 C CNN "MPN"
	1    7300 4900
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8934FC
P 7900 4800
AR Path="/5E63399D/5E8934FC" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8934FC" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8934FC" Ref="A?"  Part="1" 
AR Path="/5E8934FC" Ref="A41"  Part="1" 
F 0 "A41" H 8000 4849 50  0000 L CNN
F 1 "Solder_Pad" H 8000 4758 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 4800 50  0001 C CNN
F 3 "~" H 7900 4800 50  0001 C CNN
	1    7900 4800
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E893506
P 7550 5400
AR Path="/5E63399D/5E893506" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E893506" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E893506" Ref="SW?"  Part="1" 
AR Path="/5E893506" Ref="SW41"  Part="1" 
F 0 "SW41" H 7550 5685 50  0000 C CNN
F 1 "SW_Push" H 7550 5594 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 5600 50  0001 C CNN
F 3 "~" H 7550 5600 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 5400 50  0001 C CNN "MPN"
	1    7550 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4900 7750 4900
Wire Wire Line
	7450 4900 7400 4900
Wire Wire Line
	7200 4900 7100 4900
Wire Wire Line
	7100 4900 7100 5000
Wire Wire Line
	7150 5400 7350 5400
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E893515
P 7900 5300
AR Path="/5E63399D/5E893515" Ref="B?"  Part="1" 
AR Path="/5E634176/5E893515" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E893515" Ref="B?"  Part="1" 
AR Path="/5E893515" Ref="B41"  Part="1" 
F 0 "B41" H 8000 5349 50  0000 L CNN
F 1 "Solder_Pad" H 8000 5258 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 5300 50  0001 C CNN
F 3 "~" H 7900 5300 50  0001 C CNN
	1    7900 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 5400 7900 5400
$Comp
L dk_LED:LG_R971-KN-1 D41
U 1 1 5E893529
P 7550 4900
F 0 "D41" H 7500 4653 60  0000 C CNN
F 1 "NCD0805O1" H 7500 4759 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 5100 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 5200 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 5300 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 5400 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 5500 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 5600 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 5700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 5800 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 5900 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 6000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 6100 60  0001 L CNN "Status"
F 13 "C84262" H 7550 4900 50  0001 C CNN "LCSC"
	1    7550 4900
	-1   0    0    1   
$EndComp
Text Label 7150 5400 2    50   ~ 0
VOUT
Text Label 7100 5000 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E893535
P 7300 5950
AR Path="/5E63399D/5E893535" Ref="R?"  Part="1" 
AR Path="/5E634176/5E893535" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E893535" Ref="R?"  Part="1" 
AR Path="/5E893535" Ref="R42"  Part="1" 
F 0 "R42" V 7095 5950 50  0000 C CNN
F 1 "120 Ohm" V 7186 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 5950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 5950 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 5950 50  0001 C CNN "Description"
F 5 "C149652" H 7300 5950 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 5950 50  0001 C CNN "MPN"
	1    7300 5950
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E89353F
P 7900 5850
AR Path="/5E63399D/5E89353F" Ref="A?"  Part="1" 
AR Path="/5E634176/5E89353F" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E89353F" Ref="A?"  Part="1" 
AR Path="/5E89353F" Ref="A42"  Part="1" 
F 0 "A42" H 8000 5899 50  0000 L CNN
F 1 "Solder_Pad" H 8000 5808 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 5850 50  0001 C CNN
F 3 "~" H 7900 5850 50  0001 C CNN
	1    7900 5850
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E893549
P 7550 6450
AR Path="/5E63399D/5E893549" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E893549" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E893549" Ref="SW?"  Part="1" 
AR Path="/5E893549" Ref="SW42"  Part="1" 
F 0 "SW42" H 7550 6735 50  0000 C CNN
F 1 "SW_Push" H 7550 6644 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 6650 50  0001 C CNN
F 3 "~" H 7550 6650 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 6450 50  0001 C CNN "MPN"
	1    7550 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 5950 7750 5950
Wire Wire Line
	7450 5950 7400 5950
Wire Wire Line
	7200 5950 7100 5950
Wire Wire Line
	7100 5950 7100 6050
Wire Wire Line
	7150 6450 7350 6450
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E893558
P 7900 6350
AR Path="/5E63399D/5E893558" Ref="B?"  Part="1" 
AR Path="/5E634176/5E893558" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E893558" Ref="B?"  Part="1" 
AR Path="/5E893558" Ref="B42"  Part="1" 
F 0 "B42" H 8000 6399 50  0000 L CNN
F 1 "Solder_Pad" H 8000 6308 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 6350 50  0001 C CNN
F 3 "~" H 7900 6350 50  0001 C CNN
	1    7900 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 6450 7900 6450
$Comp
L dk_LED:LG_R971-KN-1 D42
U 1 1 5E89356C
P 7550 5950
F 0 "D42" H 7500 5703 60  0000 C CNN
F 1 "NCD0805O1" H 7500 5809 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 6150 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 6250 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 6350 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 6450 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 6550 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 6650 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 6750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 6850 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 6950 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 7050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 7150 60  0001 L CNN "Status"
F 13 "C84262" H 7550 5950 50  0001 C CNN "LCSC"
	1    7550 5950
	-1   0    0    1   
$EndComp
Text Label 7150 6450 2    50   ~ 0
VOUT
Text Label 7100 6050 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E893578
P 7300 7000
AR Path="/5E63399D/5E893578" Ref="R?"  Part="1" 
AR Path="/5E634176/5E893578" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E893578" Ref="R?"  Part="1" 
AR Path="/5E893578" Ref="R43"  Part="1" 
F 0 "R43" V 7095 7000 50  0000 C CNN
F 1 "120 Ohm" V 7186 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 7000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 7000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 7000 50  0001 C CNN "Description"
F 5 "C149652" H 7300 7000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 7000 50  0001 C CNN "MPN"
	1    7300 7000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E893582
P 7900 6900
AR Path="/5E63399D/5E893582" Ref="A?"  Part="1" 
AR Path="/5E634176/5E893582" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E893582" Ref="A?"  Part="1" 
AR Path="/5E893582" Ref="A43"  Part="1" 
F 0 "A43" H 8000 6949 50  0000 L CNN
F 1 "Solder_Pad" H 8000 6858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 6900 50  0001 C CNN
F 3 "~" H 7900 6900 50  0001 C CNN
	1    7900 6900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E89358C
P 7550 7500
AR Path="/5E63399D/5E89358C" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E89358C" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E89358C" Ref="SW?"  Part="1" 
AR Path="/5E89358C" Ref="SW43"  Part="1" 
F 0 "SW43" H 7550 7785 50  0000 C CNN
F 1 "SW_Push" H 7550 7694 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 7700 50  0001 C CNN
F 3 "~" H 7550 7700 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 7500 50  0001 C CNN "MPN"
	1    7550 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 7000 7750 7000
Wire Wire Line
	7450 7000 7400 7000
Wire Wire Line
	7200 7000 7100 7000
Wire Wire Line
	7100 7000 7100 7100
Wire Wire Line
	7150 7500 7350 7500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E89359B
P 7900 7400
AR Path="/5E63399D/5E89359B" Ref="B?"  Part="1" 
AR Path="/5E634176/5E89359B" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E89359B" Ref="B?"  Part="1" 
AR Path="/5E89359B" Ref="B43"  Part="1" 
F 0 "B43" H 8000 7449 50  0000 L CNN
F 1 "Solder_Pad" H 8000 7358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 7400 50  0001 C CNN
F 3 "~" H 7900 7400 50  0001 C CNN
	1    7900 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 7500 7900 7500
$Comp
L dk_LED:LG_R971-KN-1 D43
U 1 1 5E8935AF
P 7550 7000
F 0 "D43" H 7500 6753 60  0000 C CNN
F 1 "NCD0805O1" H 7500 6859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 7200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 7300 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 7400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 7500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 7600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 7700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 7800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 7900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 8000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 8100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 8200 60  0001 L CNN "Status"
F 13 "C84262" H 7550 7000 50  0001 C CNN "LCSC"
	1    7550 7000
	-1   0    0    1   
$EndComp
Text Label 7150 7500 2    50   ~ 0
VOUT
Text Label 7100 7100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8935BB
P 7300 8000
AR Path="/5E63399D/5E8935BB" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8935BB" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8935BB" Ref="R?"  Part="1" 
AR Path="/5E8935BB" Ref="R44"  Part="1" 
F 0 "R44" V 7095 8000 50  0000 C CNN
F 1 "120 Ohm" V 7186 8000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 8000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 8000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 8000 50  0001 C CNN "Description"
F 5 "C149652" H 7300 8000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 8000 50  0001 C CNN "MPN"
	1    7300 8000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8935C5
P 7900 7900
AR Path="/5E63399D/5E8935C5" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8935C5" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8935C5" Ref="A?"  Part="1" 
AR Path="/5E8935C5" Ref="A44"  Part="1" 
F 0 "A44" H 8000 7949 50  0000 L CNN
F 1 "Solder_Pad" H 8000 7858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 7900 50  0001 C CNN
F 3 "~" H 7900 7900 50  0001 C CNN
	1    7900 7900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8935CF
P 7550 8500
AR Path="/5E63399D/5E8935CF" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8935CF" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8935CF" Ref="SW?"  Part="1" 
AR Path="/5E8935CF" Ref="SW44"  Part="1" 
F 0 "SW44" H 7550 8785 50  0000 C CNN
F 1 "SW_Push" H 7550 8694 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 8700 50  0001 C CNN
F 3 "~" H 7550 8700 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 8500 50  0001 C CNN "MPN"
	1    7550 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 8000 7750 8000
Wire Wire Line
	7450 8000 7400 8000
Wire Wire Line
	7200 8000 7100 8000
Wire Wire Line
	7100 8000 7100 8100
Wire Wire Line
	7150 8500 7350 8500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8935DE
P 7900 8400
AR Path="/5E63399D/5E8935DE" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8935DE" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8935DE" Ref="B?"  Part="1" 
AR Path="/5E8935DE" Ref="B44"  Part="1" 
F 0 "B44" H 8000 8449 50  0000 L CNN
F 1 "Solder_Pad" H 8000 8358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 8400 50  0001 C CNN
F 3 "~" H 7900 8400 50  0001 C CNN
	1    7900 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 8500 7900 8500
$Comp
L dk_LED:LG_R971-KN-1 D44
U 1 1 5E8935F2
P 7550 8000
F 0 "D44" H 7500 7753 60  0000 C CNN
F 1 "NCD0805O1" H 7500 7859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 8200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 8300 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 8400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 8500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 8600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 8700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 8800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 8900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 9000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 9100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 9200 60  0001 L CNN "Status"
F 13 "C84262" H 7550 8000 50  0001 C CNN "LCSC"
	1    7550 8000
	-1   0    0    1   
$EndComp
Text Label 7150 8500 2    50   ~ 0
VOUT
Text Label 7100 8100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8935FE
P 7300 9050
AR Path="/5E63399D/5E8935FE" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8935FE" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8935FE" Ref="R?"  Part="1" 
AR Path="/5E8935FE" Ref="R45"  Part="1" 
F 0 "R45" V 7095 9050 50  0000 C CNN
F 1 "120 Ohm" V 7186 9050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7300 9050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 7300 9050 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 7300 9050 50  0001 C CNN "Description"
F 5 "C149652" H 7300 9050 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 7300 9050 50  0001 C CNN "MPN"
	1    7300 9050
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E893608
P 7900 8950
AR Path="/5E63399D/5E893608" Ref="A?"  Part="1" 
AR Path="/5E634176/5E893608" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E893608" Ref="A?"  Part="1" 
AR Path="/5E893608" Ref="A45"  Part="1" 
F 0 "A45" H 8000 8999 50  0000 L CNN
F 1 "Solder_Pad" H 8000 8908 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 8950 50  0001 C CNN
F 3 "~" H 7900 8950 50  0001 C CNN
	1    7900 8950
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E893612
P 7550 9550
AR Path="/5E63399D/5E893612" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E893612" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E893612" Ref="SW?"  Part="1" 
AR Path="/5E893612" Ref="SW45"  Part="1" 
F 0 "SW45" H 7550 9835 50  0000 C CNN
F 1 "SW_Push" H 7550 9744 50  0000 C CNN
F 2 "footprints:TL1105" H 7550 9750 50  0001 C CNN
F 3 "~" H 7550 9750 50  0001 C CNN
F 4 "TL1105LF250Q" H 7550 9550 50  0001 C CNN "MPN"
	1    7550 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 9050 7750 9050
Wire Wire Line
	7450 9050 7400 9050
Wire Wire Line
	7200 9050 7100 9050
Wire Wire Line
	7100 9050 7100 9150
Wire Wire Line
	7150 9550 7350 9550
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E893621
P 7900 9450
AR Path="/5E63399D/5E893621" Ref="B?"  Part="1" 
AR Path="/5E634176/5E893621" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E893621" Ref="B?"  Part="1" 
AR Path="/5E893621" Ref="B45"  Part="1" 
F 0 "B45" H 8000 9499 50  0000 L CNN
F 1 "Solder_Pad" H 8000 9408 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 7900 9450 50  0001 C CNN
F 3 "~" H 7900 9450 50  0001 C CNN
	1    7900 9450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 9550 7900 9550
$Comp
L dk_LED:LG_R971-KN-1 D45
U 1 1 5E893635
P 7550 9050
F 0 "D45" H 7500 8803 60  0000 C CNN
F 1 "NCD0805O1" H 7500 8909 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7750 9250 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 7750 9350 60  0001 L CNN
F 4 "475-1410-1-ND" H 7750 9450 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 7750 9550 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 7750 9650 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 7750 9750 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 7750 9850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 7750 9950 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 7750 10050 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 7750 10150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7750 10250 60  0001 L CNN "Status"
F 13 "C84262" H 7550 9050 50  0001 C CNN "LCSC"
	1    7550 9050
	-1   0    0    1   
$EndComp
Text Label 7150 9550 2    50   ~ 0
VOUT
Text Label 7100 9150 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BEC36
P 8950 800
AR Path="/5E63399D/5E8BEC36" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BEC36" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BEC36" Ref="R?"  Part="1" 
AR Path="/5E8BEC36" Ref="R46"  Part="1" 
F 0 "R46" V 8745 800 50  0000 C CNN
F 1 "120 Ohm" V 8836 800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 800 50  0001 C CNN "Description"
F 5 "C149652" H 8950 800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 800 50  0001 C CNN "MPN"
	1    8950 800 
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BEC40
P 9550 700
AR Path="/5E63399D/5E8BEC40" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BEC40" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BEC40" Ref="A?"  Part="1" 
AR Path="/5E8BEC40" Ref="A46"  Part="1" 
F 0 "A46" H 9650 749 50  0000 L CNN
F 1 "Solder_Pad" H 9650 658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 700 50  0001 C CNN
F 3 "~" H 9550 700 50  0001 C CNN
	1    9550 700 
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BEC4A
P 9200 1300
AR Path="/5E63399D/5E8BEC4A" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BEC4A" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BEC4A" Ref="SW?"  Part="1" 
AR Path="/5E8BEC4A" Ref="SW46"  Part="1" 
F 0 "SW46" H 9200 1585 50  0000 C CNN
F 1 "SW_Push" H 9200 1494 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 1500 50  0001 C CNN
F 3 "~" H 9200 1500 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 1300 50  0001 C CNN "MPN"
	1    9200 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 800  9400 800 
Wire Wire Line
	9100 800  9050 800 
Wire Wire Line
	8850 800  8750 800 
Wire Wire Line
	8750 800  8750 900 
Wire Wire Line
	8800 1300 9000 1300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BEC59
P 9550 1200
AR Path="/5E63399D/5E8BEC59" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BEC59" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BEC59" Ref="B?"  Part="1" 
AR Path="/5E8BEC59" Ref="B46"  Part="1" 
F 0 "B46" H 9650 1249 50  0000 L CNN
F 1 "Solder_Pad" H 9650 1158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 1200 50  0001 C CNN
F 3 "~" H 9550 1200 50  0001 C CNN
	1    9550 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 1300 9550 1300
$Comp
L dk_LED:LG_R971-KN-1 D46
U 1 1 5E8BEC6D
P 9200 800
F 0 "D46" H 9150 553 60  0000 C CNN
F 1 "NCD0805O1" H 9150 659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 1000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 1100 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 1200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 1300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 1400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 1500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 1600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 1700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 1800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 1900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 2000 60  0001 L CNN "Status"
F 13 "C84262" H 9200 800 50  0001 C CNN "LCSC"
	1    9200 800 
	-1   0    0    1   
$EndComp
Text Label 8800 1300 2    50   ~ 0
VOUT
Text Label 8750 900  2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BEC79
P 8950 1750
AR Path="/5E63399D/5E8BEC79" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BEC79" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BEC79" Ref="R?"  Part="1" 
AR Path="/5E8BEC79" Ref="R47"  Part="1" 
F 0 "R47" V 8745 1750 50  0000 C CNN
F 1 "120 Ohm" V 8836 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 1750 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 1750 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 1750 50  0001 C CNN "Description"
F 5 "C149652" H 8950 1750 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 1750 50  0001 C CNN "MPN"
	1    8950 1750
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BEC83
P 9550 1650
AR Path="/5E63399D/5E8BEC83" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BEC83" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BEC83" Ref="A?"  Part="1" 
AR Path="/5E8BEC83" Ref="A47"  Part="1" 
F 0 "A47" H 9650 1699 50  0000 L CNN
F 1 "Solder_Pad" H 9650 1608 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 1650 50  0001 C CNN
F 3 "~" H 9550 1650 50  0001 C CNN
	1    9550 1650
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BEC8D
P 9200 2250
AR Path="/5E63399D/5E8BEC8D" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BEC8D" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BEC8D" Ref="SW?"  Part="1" 
AR Path="/5E8BEC8D" Ref="SW47"  Part="1" 
F 0 "SW47" H 9200 2535 50  0000 C CNN
F 1 "SW_Push" H 9200 2444 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 2450 50  0001 C CNN
F 3 "~" H 9200 2450 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 2250 50  0001 C CNN "MPN"
	1    9200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 1750 9400 1750
Wire Wire Line
	9100 1750 9050 1750
Wire Wire Line
	8850 1750 8750 1750
Wire Wire Line
	8750 1750 8750 1850
Wire Wire Line
	8800 2250 9000 2250
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BEC9C
P 9550 2150
AR Path="/5E63399D/5E8BEC9C" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BEC9C" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BEC9C" Ref="B?"  Part="1" 
AR Path="/5E8BEC9C" Ref="B47"  Part="1" 
F 0 "B47" H 9650 2199 50  0000 L CNN
F 1 "Solder_Pad" H 9650 2108 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 2150 50  0001 C CNN
F 3 "~" H 9550 2150 50  0001 C CNN
	1    9550 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 2250 9550 2250
$Comp
L dk_LED:LG_R971-KN-1 D47
U 1 1 5E8BECB0
P 9200 1750
F 0 "D47" H 9150 1503 60  0000 C CNN
F 1 "NCD0805O1" H 9150 1609 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 1950 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 2050 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 2150 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 2250 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 2350 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 2450 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 2550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 2650 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 2750 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 2850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 2950 60  0001 L CNN "Status"
F 13 "C84262" H 9200 1750 50  0001 C CNN "LCSC"
	1    9200 1750
	-1   0    0    1   
$EndComp
Text Label 8800 2250 2    50   ~ 0
VOUT
Text Label 8750 1850 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BECBC
P 8950 2800
AR Path="/5E63399D/5E8BECBC" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BECBC" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BECBC" Ref="R?"  Part="1" 
AR Path="/5E8BECBC" Ref="R48"  Part="1" 
F 0 "R48" V 8745 2800 50  0000 C CNN
F 1 "120 Ohm" V 8836 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 2800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 2800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 2800 50  0001 C CNN "Description"
F 5 "C149652" H 8950 2800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 2800 50  0001 C CNN "MPN"
	1    8950 2800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BECC6
P 9550 2700
AR Path="/5E63399D/5E8BECC6" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BECC6" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BECC6" Ref="A?"  Part="1" 
AR Path="/5E8BECC6" Ref="A48"  Part="1" 
F 0 "A48" H 9650 2749 50  0000 L CNN
F 1 "Solder_Pad" H 9650 2658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 2700 50  0001 C CNN
F 3 "~" H 9550 2700 50  0001 C CNN
	1    9550 2700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BECD0
P 9200 3300
AR Path="/5E63399D/5E8BECD0" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BECD0" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BECD0" Ref="SW?"  Part="1" 
AR Path="/5E8BECD0" Ref="SW48"  Part="1" 
F 0 "SW48" H 9200 3585 50  0000 C CNN
F 1 "SW_Push" H 9200 3494 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 3500 50  0001 C CNN
F 3 "~" H 9200 3500 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 3300 50  0001 C CNN "MPN"
	1    9200 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 2800 9400 2800
Wire Wire Line
	9100 2800 9050 2800
Wire Wire Line
	8850 2800 8750 2800
Wire Wire Line
	8750 2800 8750 2900
Wire Wire Line
	8800 3300 9000 3300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BECDF
P 9550 3200
AR Path="/5E63399D/5E8BECDF" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BECDF" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BECDF" Ref="B?"  Part="1" 
AR Path="/5E8BECDF" Ref="B48"  Part="1" 
F 0 "B48" H 9650 3249 50  0000 L CNN
F 1 "Solder_Pad" H 9650 3158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 3200 50  0001 C CNN
F 3 "~" H 9550 3200 50  0001 C CNN
	1    9550 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 3300 9550 3300
$Comp
L dk_LED:LG_R971-KN-1 D48
U 1 1 5E8BECF3
P 9200 2800
F 0 "D48" H 9150 2553 60  0000 C CNN
F 1 "NCD0805O1" H 9150 2659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 3000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 3100 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 3200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 3300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 3400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 3500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 3600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 3700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 3800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 3900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 4000 60  0001 L CNN "Status"
F 13 "C84262" H 9200 2800 50  0001 C CNN "LCSC"
	1    9200 2800
	-1   0    0    1   
$EndComp
Text Label 8800 3300 2    50   ~ 0
VOUT
Text Label 8750 2900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BECFF
P 8950 3850
AR Path="/5E63399D/5E8BECFF" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BECFF" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BECFF" Ref="R?"  Part="1" 
AR Path="/5E8BECFF" Ref="R49"  Part="1" 
F 0 "R49" V 8745 3850 50  0000 C CNN
F 1 "120 Ohm" V 8836 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 3850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 3850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 3850 50  0001 C CNN "Description"
F 5 "C149652" H 8950 3850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 3850 50  0001 C CNN "MPN"
	1    8950 3850
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BED09
P 9550 3750
AR Path="/5E63399D/5E8BED09" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BED09" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BED09" Ref="A?"  Part="1" 
AR Path="/5E8BED09" Ref="A49"  Part="1" 
F 0 "A49" H 9650 3799 50  0000 L CNN
F 1 "Solder_Pad" H 9650 3708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 3750 50  0001 C CNN
F 3 "~" H 9550 3750 50  0001 C CNN
	1    9550 3750
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BED13
P 9200 4350
AR Path="/5E63399D/5E8BED13" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BED13" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BED13" Ref="SW?"  Part="1" 
AR Path="/5E8BED13" Ref="SW49"  Part="1" 
F 0 "SW49" H 9200 4635 50  0000 C CNN
F 1 "SW_Push" H 9200 4544 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 4550 50  0001 C CNN
F 3 "~" H 9200 4550 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 4350 50  0001 C CNN "MPN"
	1    9200 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3850 9400 3850
Wire Wire Line
	9100 3850 9050 3850
Wire Wire Line
	8850 3850 8750 3850
Wire Wire Line
	8750 3850 8750 3950
Wire Wire Line
	8800 4350 9000 4350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BED22
P 9550 4250
AR Path="/5E63399D/5E8BED22" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BED22" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BED22" Ref="B?"  Part="1" 
AR Path="/5E8BED22" Ref="B49"  Part="1" 
F 0 "B49" H 9650 4299 50  0000 L CNN
F 1 "Solder_Pad" H 9650 4208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 4250 50  0001 C CNN
F 3 "~" H 9550 4250 50  0001 C CNN
	1    9550 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 4350 9550 4350
$Comp
L dk_LED:LG_R971-KN-1 D49
U 1 1 5E8BED36
P 9200 3850
F 0 "D49" H 9150 3603 60  0000 C CNN
F 1 "NCD0805O1" H 9150 3709 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 4050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 4150 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 4350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 4450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 4550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 4850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 5050 60  0001 L CNN "Status"
F 13 "C84262" H 9200 3850 50  0001 C CNN "LCSC"
	1    9200 3850
	-1   0    0    1   
$EndComp
Text Label 8800 4350 2    50   ~ 0
VOUT
Text Label 8750 3950 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BED42
P 8950 4900
AR Path="/5E63399D/5E8BED42" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BED42" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BED42" Ref="R?"  Part="1" 
AR Path="/5E8BED42" Ref="R50"  Part="1" 
F 0 "R50" V 8745 4900 50  0000 C CNN
F 1 "120 Ohm" V 8836 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 4900 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 4900 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 4900 50  0001 C CNN "Description"
F 5 "C149652" H 8950 4900 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 4900 50  0001 C CNN "MPN"
	1    8950 4900
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BED4C
P 9550 4800
AR Path="/5E63399D/5E8BED4C" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BED4C" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BED4C" Ref="A?"  Part="1" 
AR Path="/5E8BED4C" Ref="A50"  Part="1" 
F 0 "A50" H 9650 4849 50  0000 L CNN
F 1 "Solder_Pad" H 9650 4758 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 4800 50  0001 C CNN
F 3 "~" H 9550 4800 50  0001 C CNN
	1    9550 4800
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BED56
P 9200 5400
AR Path="/5E63399D/5E8BED56" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BED56" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BED56" Ref="SW?"  Part="1" 
AR Path="/5E8BED56" Ref="SW50"  Part="1" 
F 0 "SW50" H 9200 5685 50  0000 C CNN
F 1 "SW_Push" H 9200 5594 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 5600 50  0001 C CNN
F 3 "~" H 9200 5600 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 5400 50  0001 C CNN "MPN"
	1    9200 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 4900 9400 4900
Wire Wire Line
	9100 4900 9050 4900
Wire Wire Line
	8850 4900 8750 4900
Wire Wire Line
	8750 4900 8750 5000
Wire Wire Line
	8800 5400 9000 5400
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BED65
P 9550 5300
AR Path="/5E63399D/5E8BED65" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BED65" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BED65" Ref="B?"  Part="1" 
AR Path="/5E8BED65" Ref="B50"  Part="1" 
F 0 "B50" H 9650 5349 50  0000 L CNN
F 1 "Solder_Pad" H 9650 5258 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 5300 50  0001 C CNN
F 3 "~" H 9550 5300 50  0001 C CNN
	1    9550 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 5400 9550 5400
$Comp
L dk_LED:LG_R971-KN-1 D50
U 1 1 5E8BED79
P 9200 4900
F 0 "D50" H 9150 4653 60  0000 C CNN
F 1 "NCD0805O1" H 9150 4759 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 5100 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 5200 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 5300 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 5400 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 5500 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 5600 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 5700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 5800 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 5900 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 6000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 6100 60  0001 L CNN "Status"
F 13 "C84262" H 9200 4900 50  0001 C CNN "LCSC"
	1    9200 4900
	-1   0    0    1   
$EndComp
Text Label 8800 5400 2    50   ~ 0
VOUT
Text Label 8750 5000 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BED85
P 8950 5950
AR Path="/5E63399D/5E8BED85" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BED85" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BED85" Ref="R?"  Part="1" 
AR Path="/5E8BED85" Ref="R51"  Part="1" 
F 0 "R51" V 8745 5950 50  0000 C CNN
F 1 "120 Ohm" V 8836 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 5950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 5950 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 5950 50  0001 C CNN "Description"
F 5 "C149652" H 8950 5950 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 5950 50  0001 C CNN "MPN"
	1    8950 5950
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BED8F
P 9550 5850
AR Path="/5E63399D/5E8BED8F" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BED8F" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BED8F" Ref="A?"  Part="1" 
AR Path="/5E8BED8F" Ref="A51"  Part="1" 
F 0 "A51" H 9650 5899 50  0000 L CNN
F 1 "Solder_Pad" H 9650 5808 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 5850 50  0001 C CNN
F 3 "~" H 9550 5850 50  0001 C CNN
	1    9550 5850
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BED99
P 9200 6450
AR Path="/5E63399D/5E8BED99" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BED99" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BED99" Ref="SW?"  Part="1" 
AR Path="/5E8BED99" Ref="SW51"  Part="1" 
F 0 "SW51" H 9200 6735 50  0000 C CNN
F 1 "SW_Push" H 9200 6644 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 6650 50  0001 C CNN
F 3 "~" H 9200 6650 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 6450 50  0001 C CNN "MPN"
	1    9200 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 5950 9400 5950
Wire Wire Line
	9100 5950 9050 5950
Wire Wire Line
	8850 5950 8750 5950
Wire Wire Line
	8750 5950 8750 6050
Wire Wire Line
	8800 6450 9000 6450
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BEDA8
P 9550 6350
AR Path="/5E63399D/5E8BEDA8" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BEDA8" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BEDA8" Ref="B?"  Part="1" 
AR Path="/5E8BEDA8" Ref="B51"  Part="1" 
F 0 "B51" H 9650 6399 50  0000 L CNN
F 1 "Solder_Pad" H 9650 6308 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 6350 50  0001 C CNN
F 3 "~" H 9550 6350 50  0001 C CNN
	1    9550 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 6450 9550 6450
$Comp
L dk_LED:LG_R971-KN-1 D51
U 1 1 5E8BEDBC
P 9200 5950
F 0 "D51" H 9150 5703 60  0000 C CNN
F 1 "NCD0805O1" H 9150 5809 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 6150 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 6250 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 6350 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 6450 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 6550 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 6650 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 6750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 6850 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 6950 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 7050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 7150 60  0001 L CNN "Status"
F 13 "C84262" H 9200 5950 50  0001 C CNN "LCSC"
	1    9200 5950
	-1   0    0    1   
$EndComp
Text Label 8800 6450 2    50   ~ 0
VOUT
Text Label 8750 6050 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BEDC8
P 8950 7000
AR Path="/5E63399D/5E8BEDC8" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BEDC8" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BEDC8" Ref="R?"  Part="1" 
AR Path="/5E8BEDC8" Ref="R52"  Part="1" 
F 0 "R52" V 8745 7000 50  0000 C CNN
F 1 "120 Ohm" V 8836 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 7000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 7000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 7000 50  0001 C CNN "Description"
F 5 "C149652" H 8950 7000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 7000 50  0001 C CNN "MPN"
	1    8950 7000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BEDD2
P 9550 6900
AR Path="/5E63399D/5E8BEDD2" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BEDD2" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BEDD2" Ref="A?"  Part="1" 
AR Path="/5E8BEDD2" Ref="A52"  Part="1" 
F 0 "A52" H 9650 6949 50  0000 L CNN
F 1 "Solder_Pad" H 9650 6858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 6900 50  0001 C CNN
F 3 "~" H 9550 6900 50  0001 C CNN
	1    9550 6900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BEDDC
P 9200 7500
AR Path="/5E63399D/5E8BEDDC" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BEDDC" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BEDDC" Ref="SW?"  Part="1" 
AR Path="/5E8BEDDC" Ref="SW52"  Part="1" 
F 0 "SW52" H 9200 7785 50  0000 C CNN
F 1 "SW_Push" H 9200 7694 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 7700 50  0001 C CNN
F 3 "~" H 9200 7700 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 7500 50  0001 C CNN "MPN"
	1    9200 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 7000 9400 7000
Wire Wire Line
	9100 7000 9050 7000
Wire Wire Line
	8850 7000 8750 7000
Wire Wire Line
	8750 7000 8750 7100
Wire Wire Line
	8800 7500 9000 7500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BEDEB
P 9550 7400
AR Path="/5E63399D/5E8BEDEB" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BEDEB" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BEDEB" Ref="B?"  Part="1" 
AR Path="/5E8BEDEB" Ref="B52"  Part="1" 
F 0 "B52" H 9650 7449 50  0000 L CNN
F 1 "Solder_Pad" H 9650 7358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 7400 50  0001 C CNN
F 3 "~" H 9550 7400 50  0001 C CNN
	1    9550 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 7500 9550 7500
$Comp
L dk_LED:LG_R971-KN-1 D52
U 1 1 5E8BEDFF
P 9200 7000
F 0 "D52" H 9150 6753 60  0000 C CNN
F 1 "NCD0805O1" H 9150 6859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 7200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 7300 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 7400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 7500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 7600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 7700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 7800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 7900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 8000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 8100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 8200 60  0001 L CNN "Status"
F 13 "C84262" H 9200 7000 50  0001 C CNN "LCSC"
	1    9200 7000
	-1   0    0    1   
$EndComp
Text Label 8800 7500 2    50   ~ 0
VOUT
Text Label 8750 7100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BEE0B
P 8950 8000
AR Path="/5E63399D/5E8BEE0B" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BEE0B" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BEE0B" Ref="R?"  Part="1" 
AR Path="/5E8BEE0B" Ref="R53"  Part="1" 
F 0 "R53" V 8745 8000 50  0000 C CNN
F 1 "120 Ohm" V 8836 8000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 8000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 8000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 8000 50  0001 C CNN "Description"
F 5 "C149652" H 8950 8000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 8000 50  0001 C CNN "MPN"
	1    8950 8000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BEE15
P 9550 7900
AR Path="/5E63399D/5E8BEE15" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BEE15" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BEE15" Ref="A?"  Part="1" 
AR Path="/5E8BEE15" Ref="A53"  Part="1" 
F 0 "A53" H 9650 7949 50  0000 L CNN
F 1 "Solder_Pad" H 9650 7858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 7900 50  0001 C CNN
F 3 "~" H 9550 7900 50  0001 C CNN
	1    9550 7900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BEE1F
P 9200 8500
AR Path="/5E63399D/5E8BEE1F" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BEE1F" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BEE1F" Ref="SW?"  Part="1" 
AR Path="/5E8BEE1F" Ref="SW53"  Part="1" 
F 0 "SW53" H 9200 8785 50  0000 C CNN
F 1 "SW_Push" H 9200 8694 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 8700 50  0001 C CNN
F 3 "~" H 9200 8700 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 8500 50  0001 C CNN "MPN"
	1    9200 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 8000 9400 8000
Wire Wire Line
	9100 8000 9050 8000
Wire Wire Line
	8850 8000 8750 8000
Wire Wire Line
	8750 8000 8750 8100
Wire Wire Line
	8800 8500 9000 8500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BEE2E
P 9550 8400
AR Path="/5E63399D/5E8BEE2E" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BEE2E" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BEE2E" Ref="B?"  Part="1" 
AR Path="/5E8BEE2E" Ref="B53"  Part="1" 
F 0 "B53" H 9650 8449 50  0000 L CNN
F 1 "Solder_Pad" H 9650 8358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 8400 50  0001 C CNN
F 3 "~" H 9550 8400 50  0001 C CNN
	1    9550 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 8500 9550 8500
$Comp
L dk_LED:LG_R971-KN-1 D53
U 1 1 5E8BEE42
P 9200 8000
F 0 "D53" H 9150 7753 60  0000 C CNN
F 1 "NCD0805O1" H 9150 7859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 8200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 8300 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 8400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 8500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 8600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 8700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 8800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 8900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 9000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 9100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 9200 60  0001 L CNN "Status"
F 13 "C84262" H 9200 8000 50  0001 C CNN "LCSC"
	1    9200 8000
	-1   0    0    1   
$EndComp
Text Label 8800 8500 2    50   ~ 0
VOUT
Text Label 8750 8100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E8BEE4E
P 8950 9050
AR Path="/5E63399D/5E8BEE4E" Ref="R?"  Part="1" 
AR Path="/5E634176/5E8BEE4E" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E8BEE4E" Ref="R?"  Part="1" 
AR Path="/5E8BEE4E" Ref="R54"  Part="1" 
F 0 "R54" V 8745 9050 50  0000 C CNN
F 1 "120 Ohm" V 8836 9050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8950 9050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 8950 9050 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 8950 9050 50  0001 C CNN "Description"
F 5 "C149652" H 8950 9050 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 8950 9050 50  0001 C CNN "MPN"
	1    8950 9050
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E8BEE58
P 9550 8950
AR Path="/5E63399D/5E8BEE58" Ref="A?"  Part="1" 
AR Path="/5E634176/5E8BEE58" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E8BEE58" Ref="A?"  Part="1" 
AR Path="/5E8BEE58" Ref="A54"  Part="1" 
F 0 "A54" H 9650 8999 50  0000 L CNN
F 1 "Solder_Pad" H 9650 8908 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 8950 50  0001 C CNN
F 3 "~" H 9550 8950 50  0001 C CNN
	1    9550 8950
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E8BEE62
P 9200 9550
AR Path="/5E63399D/5E8BEE62" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E8BEE62" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E8BEE62" Ref="SW?"  Part="1" 
AR Path="/5E8BEE62" Ref="SW54"  Part="1" 
F 0 "SW54" H 9200 9835 50  0000 C CNN
F 1 "SW_Push" H 9200 9744 50  0000 C CNN
F 2 "footprints:TL1105" H 9200 9750 50  0001 C CNN
F 3 "~" H 9200 9750 50  0001 C CNN
F 4 "TL1105LF250Q" H 9200 9550 50  0001 C CNN "MPN"
	1    9200 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 9050 9400 9050
Wire Wire Line
	9100 9050 9050 9050
Wire Wire Line
	8850 9050 8750 9050
Wire Wire Line
	8750 9050 8750 9150
Wire Wire Line
	8800 9550 9000 9550
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E8BEE71
P 9550 9450
AR Path="/5E63399D/5E8BEE71" Ref="B?"  Part="1" 
AR Path="/5E634176/5E8BEE71" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E8BEE71" Ref="B?"  Part="1" 
AR Path="/5E8BEE71" Ref="B54"  Part="1" 
F 0 "B54" H 9650 9499 50  0000 L CNN
F 1 "Solder_Pad" H 9650 9408 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 9550 9450 50  0001 C CNN
F 3 "~" H 9550 9450 50  0001 C CNN
	1    9550 9450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 9550 9550 9550
$Comp
L dk_LED:LG_R971-KN-1 D54
U 1 1 5E8BEE85
P 9200 9050
F 0 "D54" H 9150 8803 60  0000 C CNN
F 1 "NCD0805O1" H 9150 8909 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9400 9250 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 9400 9350 60  0001 L CNN
F 4 "475-1410-1-ND" H 9400 9450 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 9400 9550 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 9400 9650 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 9400 9750 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 9400 9850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 9400 9950 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 9400 10050 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 9400 10150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 10250 60  0001 L CNN "Status"
F 13 "C84262" H 9200 9050 50  0001 C CNN "LCSC"
	1    9200 9050
	-1   0    0    1   
$EndComp
Text Label 8800 9550 2    50   ~ 0
VOUT
Text Label 8750 9150 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90AAA9
P 10550 800
AR Path="/5E63399D/5E90AAA9" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90AAA9" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90AAA9" Ref="R?"  Part="1" 
AR Path="/5E90AAA9" Ref="R55"  Part="1" 
F 0 "R55" V 10345 800 50  0000 C CNN
F 1 "120 Ohm" V 10436 800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 800 50  0001 C CNN "Description"
F 5 "C149652" H 10550 800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 800 50  0001 C CNN "MPN"
	1    10550 800 
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90AAB3
P 11150 700
AR Path="/5E63399D/5E90AAB3" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90AAB3" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90AAB3" Ref="A?"  Part="1" 
AR Path="/5E90AAB3" Ref="A55"  Part="1" 
F 0 "A55" H 11250 749 50  0000 L CNN
F 1 "Solder_Pad" H 11250 658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 700 50  0001 C CNN
F 3 "~" H 11150 700 50  0001 C CNN
	1    11150 700 
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90AABD
P 10800 1300
AR Path="/5E63399D/5E90AABD" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90AABD" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90AABD" Ref="SW?"  Part="1" 
AR Path="/5E90AABD" Ref="SW55"  Part="1" 
F 0 "SW55" H 10800 1585 50  0000 C CNN
F 1 "SW_Push" H 10800 1494 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 1500 50  0001 C CNN
F 3 "~" H 10800 1500 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 1300 50  0001 C CNN "MPN"
	1    10800 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 800  11000 800 
Wire Wire Line
	10700 800  10650 800 
Wire Wire Line
	10450 800  10350 800 
Wire Wire Line
	10350 800  10350 900 
Wire Wire Line
	10400 1300 10600 1300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90AACC
P 11150 1200
AR Path="/5E63399D/5E90AACC" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90AACC" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90AACC" Ref="B?"  Part="1" 
AR Path="/5E90AACC" Ref="B55"  Part="1" 
F 0 "B55" H 11250 1249 50  0000 L CNN
F 1 "Solder_Pad" H 11250 1158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 1200 50  0001 C CNN
F 3 "~" H 11150 1200 50  0001 C CNN
	1    11150 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 1300 11150 1300
$Comp
L dk_LED:LG_R971-KN-1 D55
U 1 1 5E90AAE0
P 10800 800
F 0 "D55" H 10750 553 60  0000 C CNN
F 1 "NCD0805O1" H 10750 659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 1000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 1100 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 1200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 1300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 1400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 1500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 1600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 1700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 1800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 1900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 2000 60  0001 L CNN "Status"
F 13 "C84262" H 10800 800 50  0001 C CNN "LCSC"
	1    10800 800 
	-1   0    0    1   
$EndComp
Text Label 10400 1300 2    50   ~ 0
VOUT
Text Label 10350 900  2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90AAEC
P 10550 1750
AR Path="/5E63399D/5E90AAEC" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90AAEC" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90AAEC" Ref="R?"  Part="1" 
AR Path="/5E90AAEC" Ref="R56"  Part="1" 
F 0 "R56" V 10345 1750 50  0000 C CNN
F 1 "120 Ohm" V 10436 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 1750 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 1750 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 1750 50  0001 C CNN "Description"
F 5 "C149652" H 10550 1750 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 1750 50  0001 C CNN "MPN"
	1    10550 1750
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90AAF6
P 11150 1650
AR Path="/5E63399D/5E90AAF6" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90AAF6" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90AAF6" Ref="A?"  Part="1" 
AR Path="/5E90AAF6" Ref="A56"  Part="1" 
F 0 "A56" H 11250 1699 50  0000 L CNN
F 1 "Solder_Pad" H 11250 1608 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 1650 50  0001 C CNN
F 3 "~" H 11150 1650 50  0001 C CNN
	1    11150 1650
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90AB00
P 10800 2250
AR Path="/5E63399D/5E90AB00" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90AB00" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90AB00" Ref="SW?"  Part="1" 
AR Path="/5E90AB00" Ref="SW56"  Part="1" 
F 0 "SW56" H 10800 2535 50  0000 C CNN
F 1 "SW_Push" H 10800 2444 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 2450 50  0001 C CNN
F 3 "~" H 10800 2450 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 2250 50  0001 C CNN "MPN"
	1    10800 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 1750 11000 1750
Wire Wire Line
	10700 1750 10650 1750
Wire Wire Line
	10450 1750 10350 1750
Wire Wire Line
	10350 1750 10350 1850
Wire Wire Line
	10400 2250 10600 2250
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90AB0F
P 11150 2150
AR Path="/5E63399D/5E90AB0F" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90AB0F" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90AB0F" Ref="B?"  Part="1" 
AR Path="/5E90AB0F" Ref="B56"  Part="1" 
F 0 "B56" H 11250 2199 50  0000 L CNN
F 1 "Solder_Pad" H 11250 2108 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 2150 50  0001 C CNN
F 3 "~" H 11150 2150 50  0001 C CNN
	1    11150 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 2250 11150 2250
$Comp
L dk_LED:LG_R971-KN-1 D56
U 1 1 5E90AB23
P 10800 1750
F 0 "D56" H 10750 1503 60  0000 C CNN
F 1 "NCD0805O1" H 10750 1609 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 1950 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 2050 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 2150 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 2250 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 2350 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 2450 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 2550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 2650 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 2750 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 2850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 2950 60  0001 L CNN "Status"
F 13 "C84262" H 10800 1750 50  0001 C CNN "LCSC"
	1    10800 1750
	-1   0    0    1   
$EndComp
Text Label 10400 2250 2    50   ~ 0
VOUT
Text Label 10350 1850 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90AB2F
P 10550 2800
AR Path="/5E63399D/5E90AB2F" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90AB2F" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90AB2F" Ref="R?"  Part="1" 
AR Path="/5E90AB2F" Ref="R57"  Part="1" 
F 0 "R57" V 10345 2800 50  0000 C CNN
F 1 "120 Ohm" V 10436 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 2800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 2800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 2800 50  0001 C CNN "Description"
F 5 "C149652" H 10550 2800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 2800 50  0001 C CNN "MPN"
	1    10550 2800
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90AB39
P 11150 2700
AR Path="/5E63399D/5E90AB39" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90AB39" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90AB39" Ref="A?"  Part="1" 
AR Path="/5E90AB39" Ref="A57"  Part="1" 
F 0 "A57" H 11250 2749 50  0000 L CNN
F 1 "Solder_Pad" H 11250 2658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 2700 50  0001 C CNN
F 3 "~" H 11150 2700 50  0001 C CNN
	1    11150 2700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90AB43
P 10800 3300
AR Path="/5E63399D/5E90AB43" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90AB43" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90AB43" Ref="SW?"  Part="1" 
AR Path="/5E90AB43" Ref="SW57"  Part="1" 
F 0 "SW57" H 10800 3585 50  0000 C CNN
F 1 "SW_Push" H 10800 3494 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 3500 50  0001 C CNN
F 3 "~" H 10800 3500 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 3300 50  0001 C CNN "MPN"
	1    10800 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 2800 11000 2800
Wire Wire Line
	10700 2800 10650 2800
Wire Wire Line
	10450 2800 10350 2800
Wire Wire Line
	10350 2800 10350 2900
Wire Wire Line
	10400 3300 10600 3300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90AB52
P 11150 3200
AR Path="/5E63399D/5E90AB52" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90AB52" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90AB52" Ref="B?"  Part="1" 
AR Path="/5E90AB52" Ref="B57"  Part="1" 
F 0 "B57" H 11250 3249 50  0000 L CNN
F 1 "Solder_Pad" H 11250 3158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 3200 50  0001 C CNN
F 3 "~" H 11150 3200 50  0001 C CNN
	1    11150 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 3300 11150 3300
$Comp
L dk_LED:LG_R971-KN-1 D57
U 1 1 5E90AB66
P 10800 2800
F 0 "D57" H 10750 2553 60  0000 C CNN
F 1 "NCD0805O1" H 10750 2659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 3000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 3100 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 3200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 3300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 3400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 3500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 3600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 3700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 3800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 3900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 4000 60  0001 L CNN "Status"
F 13 "C84262" H 10800 2800 50  0001 C CNN "LCSC"
	1    10800 2800
	-1   0    0    1   
$EndComp
Text Label 10400 3300 2    50   ~ 0
VOUT
Text Label 10350 2900 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90AB72
P 10550 3850
AR Path="/5E63399D/5E90AB72" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90AB72" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90AB72" Ref="R?"  Part="1" 
AR Path="/5E90AB72" Ref="R58"  Part="1" 
F 0 "R58" V 10345 3850 50  0000 C CNN
F 1 "120 Ohm" V 10436 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 3850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 3850 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 3850 50  0001 C CNN "Description"
F 5 "C149652" H 10550 3850 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 3850 50  0001 C CNN "MPN"
	1    10550 3850
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90AB7C
P 11150 3750
AR Path="/5E63399D/5E90AB7C" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90AB7C" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90AB7C" Ref="A?"  Part="1" 
AR Path="/5E90AB7C" Ref="A58"  Part="1" 
F 0 "A58" H 11250 3799 50  0000 L CNN
F 1 "Solder_Pad" H 11250 3708 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 3750 50  0001 C CNN
F 3 "~" H 11150 3750 50  0001 C CNN
	1    11150 3750
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90AB86
P 10800 4350
AR Path="/5E63399D/5E90AB86" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90AB86" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90AB86" Ref="SW?"  Part="1" 
AR Path="/5E90AB86" Ref="SW58"  Part="1" 
F 0 "SW58" H 10800 4635 50  0000 C CNN
F 1 "SW_Push" H 10800 4544 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 4550 50  0001 C CNN
F 3 "~" H 10800 4550 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 4350 50  0001 C CNN "MPN"
	1    10800 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 3850 11000 3850
Wire Wire Line
	10700 3850 10650 3850
Wire Wire Line
	10450 3850 10350 3850
Wire Wire Line
	10350 3850 10350 3950
Wire Wire Line
	10400 4350 10600 4350
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90AB95
P 11150 4250
AR Path="/5E63399D/5E90AB95" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90AB95" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90AB95" Ref="B?"  Part="1" 
AR Path="/5E90AB95" Ref="B58"  Part="1" 
F 0 "B58" H 11250 4299 50  0000 L CNN
F 1 "Solder_Pad" H 11250 4208 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 4250 50  0001 C CNN
F 3 "~" H 11150 4250 50  0001 C CNN
	1    11150 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 4350 11150 4350
$Comp
L dk_LED:LG_R971-KN-1 D58
U 1 1 5E90ABA9
P 10800 3850
F 0 "D58" H 10750 3603 60  0000 C CNN
F 1 "NCD0805O1" H 10750 3709 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 4050 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 4150 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 4350 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 4450 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 4550 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 4850 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 5050 60  0001 L CNN "Status"
F 13 "C84262" H 10800 3850 50  0001 C CNN "LCSC"
	1    10800 3850
	-1   0    0    1   
$EndComp
Text Label 10400 4350 2    50   ~ 0
VOUT
Text Label 10350 3950 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90ABB5
P 10550 4900
AR Path="/5E63399D/5E90ABB5" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90ABB5" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90ABB5" Ref="R?"  Part="1" 
AR Path="/5E90ABB5" Ref="R59"  Part="1" 
F 0 "R59" V 10345 4900 50  0000 C CNN
F 1 "120 Ohm" V 10436 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 4900 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 4900 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 4900 50  0001 C CNN "Description"
F 5 "C149652" H 10550 4900 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 4900 50  0001 C CNN "MPN"
	1    10550 4900
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90ABBF
P 11150 4800
AR Path="/5E63399D/5E90ABBF" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90ABBF" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90ABBF" Ref="A?"  Part="1" 
AR Path="/5E90ABBF" Ref="A59"  Part="1" 
F 0 "A59" H 11250 4849 50  0000 L CNN
F 1 "Solder_Pad" H 11250 4758 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 4800 50  0001 C CNN
F 3 "~" H 11150 4800 50  0001 C CNN
	1    11150 4800
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90ABC9
P 10800 5400
AR Path="/5E63399D/5E90ABC9" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90ABC9" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90ABC9" Ref="SW?"  Part="1" 
AR Path="/5E90ABC9" Ref="SW59"  Part="1" 
F 0 "SW59" H 10800 5685 50  0000 C CNN
F 1 "SW_Push" H 10800 5594 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 5600 50  0001 C CNN
F 3 "~" H 10800 5600 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 5400 50  0001 C CNN "MPN"
	1    10800 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 4900 11000 4900
Wire Wire Line
	10700 4900 10650 4900
Wire Wire Line
	10450 4900 10350 4900
Wire Wire Line
	10350 4900 10350 5000
Wire Wire Line
	10400 5400 10600 5400
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90ABD8
P 11150 5300
AR Path="/5E63399D/5E90ABD8" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90ABD8" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90ABD8" Ref="B?"  Part="1" 
AR Path="/5E90ABD8" Ref="B59"  Part="1" 
F 0 "B59" H 11250 5349 50  0000 L CNN
F 1 "Solder_Pad" H 11250 5258 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 5300 50  0001 C CNN
F 3 "~" H 11150 5300 50  0001 C CNN
	1    11150 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 5400 11150 5400
$Comp
L dk_LED:LG_R971-KN-1 D59
U 1 1 5E90ABEC
P 10800 4900
F 0 "D59" H 10750 4653 60  0000 C CNN
F 1 "NCD0805O1" H 10750 4759 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 5100 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 5200 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 5300 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 5400 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 5500 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 5600 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 5700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 5800 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 5900 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 6000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 6100 60  0001 L CNN "Status"
F 13 "C84262" H 10800 4900 50  0001 C CNN "LCSC"
	1    10800 4900
	-1   0    0    1   
$EndComp
Text Label 10400 5400 2    50   ~ 0
VOUT
Text Label 10350 5000 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90ABF8
P 10550 5950
AR Path="/5E63399D/5E90ABF8" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90ABF8" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90ABF8" Ref="R?"  Part="1" 
AR Path="/5E90ABF8" Ref="R60"  Part="1" 
F 0 "R60" V 10345 5950 50  0000 C CNN
F 1 "120 Ohm" V 10436 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 5950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 5950 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 5950 50  0001 C CNN "Description"
F 5 "C149652" H 10550 5950 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 5950 50  0001 C CNN "MPN"
	1    10550 5950
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90AC02
P 11150 5850
AR Path="/5E63399D/5E90AC02" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90AC02" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90AC02" Ref="A?"  Part="1" 
AR Path="/5E90AC02" Ref="A60"  Part="1" 
F 0 "A60" H 11250 5899 50  0000 L CNN
F 1 "Solder_Pad" H 11250 5808 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 5850 50  0001 C CNN
F 3 "~" H 11150 5850 50  0001 C CNN
	1    11150 5850
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90AC0C
P 10800 6450
AR Path="/5E63399D/5E90AC0C" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90AC0C" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90AC0C" Ref="SW?"  Part="1" 
AR Path="/5E90AC0C" Ref="SW60"  Part="1" 
F 0 "SW60" H 10800 6735 50  0000 C CNN
F 1 "SW_Push" H 10800 6644 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 6650 50  0001 C CNN
F 3 "~" H 10800 6650 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 6450 50  0001 C CNN "MPN"
	1    10800 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 5950 11000 5950
Wire Wire Line
	10700 5950 10650 5950
Wire Wire Line
	10450 5950 10350 5950
Wire Wire Line
	10350 5950 10350 6050
Wire Wire Line
	10400 6450 10600 6450
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90AC1B
P 11150 6350
AR Path="/5E63399D/5E90AC1B" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90AC1B" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90AC1B" Ref="B?"  Part="1" 
AR Path="/5E90AC1B" Ref="B60"  Part="1" 
F 0 "B60" H 11250 6399 50  0000 L CNN
F 1 "Solder_Pad" H 11250 6308 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 6350 50  0001 C CNN
F 3 "~" H 11150 6350 50  0001 C CNN
	1    11150 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 6450 11150 6450
$Comp
L dk_LED:LG_R971-KN-1 D60
U 1 1 5E90AC2F
P 10800 5950
F 0 "D60" H 10750 5703 60  0000 C CNN
F 1 "NCD0805O1" H 10750 5809 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 6150 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 6250 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 6350 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 6450 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 6550 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 6650 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 6750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 6850 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 6950 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 7050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 7150 60  0001 L CNN "Status"
F 13 "C84262" H 10800 5950 50  0001 C CNN "LCSC"
	1    10800 5950
	-1   0    0    1   
$EndComp
Text Label 10400 6450 2    50   ~ 0
VOUT
Text Label 10350 6050 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90AC3B
P 10550 7000
AR Path="/5E63399D/5E90AC3B" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90AC3B" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90AC3B" Ref="R?"  Part="1" 
AR Path="/5E90AC3B" Ref="R61"  Part="1" 
F 0 "R61" V 10345 7000 50  0000 C CNN
F 1 "120 Ohm" V 10436 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 7000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 7000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 7000 50  0001 C CNN "Description"
F 5 "C149652" H 10550 7000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 7000 50  0001 C CNN "MPN"
	1    10550 7000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90AC45
P 11150 6900
AR Path="/5E63399D/5E90AC45" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90AC45" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90AC45" Ref="A?"  Part="1" 
AR Path="/5E90AC45" Ref="A61"  Part="1" 
F 0 "A61" H 11250 6949 50  0000 L CNN
F 1 "Solder_Pad" H 11250 6858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 6900 50  0001 C CNN
F 3 "~" H 11150 6900 50  0001 C CNN
	1    11150 6900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90AC4F
P 10800 7500
AR Path="/5E63399D/5E90AC4F" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90AC4F" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90AC4F" Ref="SW?"  Part="1" 
AR Path="/5E90AC4F" Ref="SW61"  Part="1" 
F 0 "SW61" H 10800 7785 50  0000 C CNN
F 1 "SW_Push" H 10800 7694 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 7700 50  0001 C CNN
F 3 "~" H 10800 7700 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 7500 50  0001 C CNN "MPN"
	1    10800 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 7000 11000 7000
Wire Wire Line
	10700 7000 10650 7000
Wire Wire Line
	10450 7000 10350 7000
Wire Wire Line
	10350 7000 10350 7100
Wire Wire Line
	10400 7500 10600 7500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90AC5E
P 11150 7400
AR Path="/5E63399D/5E90AC5E" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90AC5E" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90AC5E" Ref="B?"  Part="1" 
AR Path="/5E90AC5E" Ref="B61"  Part="1" 
F 0 "B61" H 11250 7449 50  0000 L CNN
F 1 "Solder_Pad" H 11250 7358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 7400 50  0001 C CNN
F 3 "~" H 11150 7400 50  0001 C CNN
	1    11150 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 7500 11150 7500
$Comp
L dk_LED:LG_R971-KN-1 D61
U 1 1 5E90AC72
P 10800 7000
F 0 "D61" H 10750 6753 60  0000 C CNN
F 1 "NCD0805O1" H 10750 6859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 7200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 7300 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 7400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 7500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 7600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 7700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 7800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 7900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 8000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 8100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 8200 60  0001 L CNN "Status"
F 13 "C84262" H 10800 7000 50  0001 C CNN "LCSC"
	1    10800 7000
	-1   0    0    1   
$EndComp
Text Label 10400 7500 2    50   ~ 0
VOUT
Text Label 10350 7100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90AC7E
P 10550 8000
AR Path="/5E63399D/5E90AC7E" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90AC7E" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90AC7E" Ref="R?"  Part="1" 
AR Path="/5E90AC7E" Ref="R62"  Part="1" 
F 0 "R62" V 10345 8000 50  0000 C CNN
F 1 "120 Ohm" V 10436 8000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 8000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 8000 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 8000 50  0001 C CNN "Description"
F 5 "C149652" H 10550 8000 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 8000 50  0001 C CNN "MPN"
	1    10550 8000
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90AC88
P 11150 7900
AR Path="/5E63399D/5E90AC88" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90AC88" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90AC88" Ref="A?"  Part="1" 
AR Path="/5E90AC88" Ref="A62"  Part="1" 
F 0 "A62" H 11250 7949 50  0000 L CNN
F 1 "Solder_Pad" H 11250 7858 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 7900 50  0001 C CNN
F 3 "~" H 11150 7900 50  0001 C CNN
	1    11150 7900
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90AC92
P 10800 8500
AR Path="/5E63399D/5E90AC92" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90AC92" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90AC92" Ref="SW?"  Part="1" 
AR Path="/5E90AC92" Ref="SW62"  Part="1" 
F 0 "SW62" H 10800 8785 50  0000 C CNN
F 1 "SW_Push" H 10800 8694 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 8700 50  0001 C CNN
F 3 "~" H 10800 8700 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 8500 50  0001 C CNN "MPN"
	1    10800 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 8000 11000 8000
Wire Wire Line
	10700 8000 10650 8000
Wire Wire Line
	10450 8000 10350 8000
Wire Wire Line
	10350 8000 10350 8100
Wire Wire Line
	10400 8500 10600 8500
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90ACA1
P 11150 8400
AR Path="/5E63399D/5E90ACA1" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90ACA1" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90ACA1" Ref="B?"  Part="1" 
AR Path="/5E90ACA1" Ref="B62"  Part="1" 
F 0 "B62" H 11250 8449 50  0000 L CNN
F 1 "Solder_Pad" H 11250 8358 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 8400 50  0001 C CNN
F 3 "~" H 11150 8400 50  0001 C CNN
	1    11150 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 8500 11150 8500
$Comp
L dk_LED:LG_R971-KN-1 D62
U 1 1 5E90ACB5
P 10800 8000
F 0 "D62" H 10750 7753 60  0000 C CNN
F 1 "NCD0805O1" H 10750 7859 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 8200 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 8300 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 8400 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 8500 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 8600 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 8700 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 8800 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 8900 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 9000 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 9100 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 9200 60  0001 L CNN "Status"
F 13 "C84262" H 10800 8000 50  0001 C CNN "LCSC"
	1    10800 8000
	-1   0    0    1   
$EndComp
Text Label 10400 8500 2    50   ~ 0
VOUT
Text Label 10350 8100 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E90ACC1
P 10550 9050
AR Path="/5E63399D/5E90ACC1" Ref="R?"  Part="1" 
AR Path="/5E634176/5E90ACC1" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E90ACC1" Ref="R?"  Part="1" 
AR Path="/5E90ACC1" Ref="R63"  Part="1" 
F 0 "R63" V 10345 9050 50  0000 C CNN
F 1 "120 Ohm" V 10436 9050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 10550 9050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 10550 9050 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 10550 9050 50  0001 C CNN "Description"
F 5 "C149652" H 10550 9050 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 10550 9050 50  0001 C CNN "MPN"
	1    10550 9050
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E90ACCB
P 11150 8950
AR Path="/5E63399D/5E90ACCB" Ref="A?"  Part="1" 
AR Path="/5E634176/5E90ACCB" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E90ACCB" Ref="A?"  Part="1" 
AR Path="/5E90ACCB" Ref="A63"  Part="1" 
F 0 "A63" H 11250 8999 50  0000 L CNN
F 1 "Solder_Pad" H 11250 8908 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 8950 50  0001 C CNN
F 3 "~" H 11150 8950 50  0001 C CNN
	1    11150 8950
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E90ACD5
P 10800 9550
AR Path="/5E63399D/5E90ACD5" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E90ACD5" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E90ACD5" Ref="SW?"  Part="1" 
AR Path="/5E90ACD5" Ref="SW63"  Part="1" 
F 0 "SW63" H 10800 9835 50  0000 C CNN
F 1 "SW_Push" H 10800 9744 50  0000 C CNN
F 2 "footprints:TL1105" H 10800 9750 50  0001 C CNN
F 3 "~" H 10800 9750 50  0001 C CNN
F 4 "TL1105LF250Q" H 10800 9550 50  0001 C CNN "MPN"
	1    10800 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 9050 11000 9050
Wire Wire Line
	10700 9050 10650 9050
Wire Wire Line
	10450 9050 10350 9050
Wire Wire Line
	10350 9050 10350 9150
Wire Wire Line
	10400 9550 10600 9550
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E90ACE4
P 11150 9450
AR Path="/5E63399D/5E90ACE4" Ref="B?"  Part="1" 
AR Path="/5E634176/5E90ACE4" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E90ACE4" Ref="B?"  Part="1" 
AR Path="/5E90ACE4" Ref="B63"  Part="1" 
F 0 "B63" H 11250 9499 50  0000 L CNN
F 1 "Solder_Pad" H 11250 9408 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 11150 9450 50  0001 C CNN
F 3 "~" H 11150 9450 50  0001 C CNN
	1    11150 9450
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 9550 11150 9550
$Comp
L dk_LED:LG_R971-KN-1 D63
U 1 1 5E90ACF8
P 10800 9050
F 0 "D63" H 10750 8803 60  0000 C CNN
F 1 "NCD0805O1" H 10750 8909 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 11000 9250 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 11000 9350 60  0001 L CNN
F 4 "475-1410-1-ND" H 11000 9450 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 11000 9550 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 11000 9650 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 11000 9750 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 11000 9850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 11000 9950 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 11000 10050 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 11000 10150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11000 10250 60  0001 L CNN "Status"
F 13 "C84262" H 10800 9050 50  0001 C CNN "LCSC"
	1    10800 9050
	-1   0    0    1   
$EndComp
Text Label 10400 9550 2    50   ~ 0
VOUT
Text Label 10350 9150 2    50   ~ 0
GND
$Comp
L ledbox-rescue:R_Small_US-Device R?
U 1 1 5E919A65
P 12150 800
AR Path="/5E63399D/5E919A65" Ref="R?"  Part="1" 
AR Path="/5E634176/5E919A65" Ref="R?"  Part="1" 
AR Path="/5E6426EF/5E919A65" Ref="R?"  Part="1" 
AR Path="/5E919A65" Ref="R64"  Part="1" 
F 0 "R64" V 11945 800 50  0000 C CNN
F 1 "120 Ohm" V 12036 800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 12150 800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1811141320_Ever-Ohms-Tech-CRH0805J120RP05Z_C149652.pdf" H 12150 800 50  0001 C CNN
F 4 "±5% ±100PPM/℃ 120Ω 0.25W 0805 CHIP RESISTOR - SURFACE MOUNT ROHS" H 12150 800 50  0001 C CNN "Description"
F 5 "C149652" H 12150 800 50  0001 C CNN "LCSC"
F 6 "CRH0805J120RP05ZCopy" H 12150 800 50  0001 C CNN "MPN"
	1    12150 800 
	0    1    1    0   
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical A?
U 1 1 5E919A6F
P 12750 700
AR Path="/5E63399D/5E919A6F" Ref="A?"  Part="1" 
AR Path="/5E634176/5E919A6F" Ref="A?"  Part="1" 
AR Path="/5E6426EF/5E919A6F" Ref="A?"  Part="1" 
AR Path="/5E919A6F" Ref="A64"  Part="1" 
F 0 "A64" H 12850 749 50  0000 L CNN
F 1 "Solder_Pad" H 12850 658 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 12750 700 50  0001 C CNN
F 3 "~" H 12750 700 50  0001 C CNN
	1    12750 700 
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:SW_Push-Switch SW?
U 1 1 5E919A79
P 12400 1300
AR Path="/5E63399D/5E919A79" Ref="SW?"  Part="1" 
AR Path="/5E634176/5E919A79" Ref="SW?"  Part="1" 
AR Path="/5E6426EF/5E919A79" Ref="SW?"  Part="1" 
AR Path="/5E919A79" Ref="SW64"  Part="1" 
F 0 "SW64" H 12400 1585 50  0000 C CNN
F 1 "SW_Push" H 12400 1494 50  0000 C CNN
F 2 "footprints:TL1105" H 12400 1500 50  0001 C CNN
F 3 "~" H 12400 1500 50  0001 C CNN
F 4 "TL1105LF250Q" H 12400 1300 50  0001 C CNN "MPN"
	1    12400 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	12750 800  12600 800 
Wire Wire Line
	12300 800  12250 800 
Wire Wire Line
	12050 800  11950 800 
Wire Wire Line
	11950 800  11950 900 
Wire Wire Line
	12000 1300 12200 1300
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical B?
U 1 1 5E919A88
P 12750 1200
AR Path="/5E63399D/5E919A88" Ref="B?"  Part="1" 
AR Path="/5E634176/5E919A88" Ref="B?"  Part="1" 
AR Path="/5E6426EF/5E919A88" Ref="B?"  Part="1" 
AR Path="/5E919A88" Ref="B64"  Part="1" 
F 0 "B64" H 12850 1249 50  0000 L CNN
F 1 "Solder_Pad" H 12850 1158 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 12750 1200 50  0001 C CNN
F 3 "~" H 12750 1200 50  0001 C CNN
	1    12750 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	12600 1300 12750 1300
$Comp
L dk_LED:LG_R971-KN-1 D64
U 1 1 5E919A9C
P 12400 800
F 0 "D64" H 12350 553 60  0000 C CNN
F 1 "NCD0805O1" H 12350 659 60  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 12600 1000 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/lcsc/2008201032_Foshan-NationStar-Optoelectronics-NCD0805O1_C84262.pdf" H 12600 1100 60  0001 L CNN
F 4 "475-1410-1-ND" H 12600 1200 60  0001 L CNN "Digi-Key_PN"
F 5 "LG R971-KN-1" H 12600 1300 60  0001 L CNN "MPN"
F 6 "Optoelectronics" H 12600 1400 60  0001 L CNN "Category"
F 7 "LED Indication - Discrete" H 12600 1500 60  0001 L CNN "Family"
F 8 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 12600 1600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 12600 1700 60  0001 L CNN "DK_Detail_Page"
F 10 "ORANGE 589~610NM 0805 LIGHT EMITTING DIODES (LED) ROHS" H 12600 1800 60  0001 L CNN "Description"
F 11 "OSRAM Opto Semiconductors Inc." H 12600 1900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 12600 2000 60  0001 L CNN "Status"
F 13 "C84262" H 12400 800 50  0001 C CNN "LCSC"
	1    12400 800 
	-1   0    0    1   
$EndComp
Text Label 12000 1300 2    50   ~ 0
VOUT
Text Label 11950 900  2    50   ~ 0
GND
$Comp
L ul_MCP1700-5002E-TO:MCP1700-5002E_TO U1
U 1 1 613D7D69
P 12950 8700
F 0 "U1" H 14050 9087 60  0000 C CNN
F 1 "MCP1700-5002E_TO" H 14050 8981 60  0000 C CNN
F 2 "ul_MCP1700-5002E-TO:MCP1700-5002E&slash_TO" H 14050 8940 60  0001 C CNN
F 3 "" H 12950 8700 60  0000 C CNN
	1    12950 8700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:GND-power #PWR0102
U 1 1 5E72CB5C
P 12500 8700
F 0 "#PWR0102" H 12500 8450 50  0001 C CNN
F 1 "GND" H 12505 8527 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_SMD_5x10mm" H 12500 8700 50  0001 C CNN
F 3 "" H 12500 8700 50  0001 C CNN
	1    12500 8700
	1    0    0    -1  
$EndComp
$Comp
L ledbox-rescue:MountingHole_Pad-Mechanical H2
U 1 1 5E73C468
P 12500 8350
F 0 "H2" H 12600 8399 50  0000 L CNN
F 1 "MountingHole_Pad" H 12600 8308 50  0000 L CNN
F 2 "footprints:SolderWirePad_single_1-5mmDrill" H 12500 8350 50  0001 C CNN
F 3 "~" H 12500 8350 50  0001 C CNN
	1    12500 8350
	1    0    0    -1  
$EndComp
Wire Wire Line
	12950 8700 12500 8700
Wire Wire Line
	12500 8700 12500 8450
Connection ~ 12500 8700
$Comp
L ledbox-rescue:+9V-power #PWR0101
U 1 1 5E72D12B
P 15300 8900
F 0 "#PWR0101" H 15300 8750 50  0001 C CNN
F 1 "+9V" H 15200 8900 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_SMD_5x10mm" H 15300 8900 50  0001 C CNN
F 3 "" H 15300 8900 50  0001 C CNN
	1    15300 8900
	-1   0    0    1   
$EndComp
Wire Wire Line
	15300 8800 15150 8800
Wire Wire Line
	15700 8800 15300 8800
Connection ~ 15300 8800
Wire Wire Line
	15300 8800 15300 8900
Text Label 1450 850  3    50   ~ 0
TEST
Text Label 1450 1800 3    50   ~ 0
TEST
Text Label 1450 2800 3    50   ~ 0
TEST
Text Label 1450 3850 3    50   ~ 0
TEST
Text Label 1450 4900 3    50   ~ 0
TEST
Text Label 1450 5950 3    50   ~ 0
TEST
Text Label 1450 7000 3    50   ~ 0
TEST
Text Label 1450 8000 3    50   ~ 0
TEST
Text Label 1450 9050 3    50   ~ 0
TEST
Text Label 3050 800  3    50   ~ 0
TEST
Text Label 3050 1750 3    50   ~ 0
TEST
Text Label 3050 2800 3    50   ~ 0
TEST
Text Label 3050 3850 3    50   ~ 0
TEST
Text Label 3050 4900 3    50   ~ 0
TEST
Text Label 3050 5950 3    50   ~ 0
TEST
Text Label 3050 7000 3    50   ~ 0
TEST
Text Label 3050 8000 3    50   ~ 0
TEST
Text Label 3050 9050 3    50   ~ 0
TEST
Text Label 4700 850  3    50   ~ 0
TEST
Text Label 4700 1800 3    50   ~ 0
TEST
Text Label 4700 2800 3    50   ~ 0
TEST
Text Label 4700 3850 3    50   ~ 0
TEST
Text Label 4700 4900 3    50   ~ 0
TEST
Text Label 4700 5950 3    50   ~ 0
TEST
Text Label 4700 7000 3    50   ~ 0
TEST
Text Label 4700 8000 3    50   ~ 0
TEST
Text Label 4700 9050 3    50   ~ 0
TEST
Text Label 6300 800  3    50   ~ 0
TEST
Text Label 6300 1750 3    50   ~ 0
TEST
Text Label 6300 2800 3    50   ~ 0
TEST
Text Label 6300 3850 3    50   ~ 0
TEST
Text Label 6300 4900 3    50   ~ 0
TEST
Text Label 6300 5950 3    50   ~ 0
TEST
Text Label 6300 7000 3    50   ~ 0
TEST
Text Label 6300 8000 3    50   ~ 0
TEST
Text Label 6300 9050 3    50   ~ 0
TEST
Text Label 7900 800  3    50   ~ 0
TEST
Text Label 7900 1750 3    50   ~ 0
TEST
Text Label 7900 2800 3    50   ~ 0
TEST
Text Label 7900 3850 3    50   ~ 0
TEST
Text Label 7900 4900 3    50   ~ 0
TEST
Text Label 7900 5950 3    50   ~ 0
TEST
Text Label 7900 7000 3    50   ~ 0
TEST
Text Label 7900 8000 3    50   ~ 0
TEST
Text Label 7900 9050 3    50   ~ 0
TEST
Text Label 9550 800  3    50   ~ 0
TEST
Text Label 9550 1750 3    50   ~ 0
TEST
Text Label 9550 2800 3    50   ~ 0
TEST
Text Label 9550 3850 3    50   ~ 0
TEST
Text Label 9550 4900 3    50   ~ 0
TEST
Text Label 9550 5950 3    50   ~ 0
TEST
Text Label 9550 7000 3    50   ~ 0
TEST
Text Label 9550 8000 3    50   ~ 0
TEST
Text Label 9550 9050 3    50   ~ 0
TEST
Text Label 11150 800  3    50   ~ 0
TEST
Text Label 11150 1750 3    50   ~ 0
TEST
Text Label 11150 2800 3    50   ~ 0
TEST
Text Label 11150 3850 3    50   ~ 0
TEST
Text Label 11150 4900 3    50   ~ 0
TEST
Text Label 11150 5950 3    50   ~ 0
TEST
Text Label 11150 7000 3    50   ~ 0
TEST
Text Label 11150 8000 3    50   ~ 0
TEST
Text Label 11150 9050 3    50   ~ 0
TEST
Text Label 12750 800  3    50   ~ 0
TEST
$Comp
L Jumpers:Jumper_2_Open JP1
U 1 1 615ACD98
P 14550 7700
F 0 "JP1" H 14550 7935 50  0000 C CNN
F 1 "TEST" H 14550 7844 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 14550 7700 50  0001 C CNN
F 3 "~" H 14550 7700 50  0001 C CNN
	1    14550 7700
	1    0    0    -1  
$EndComp
Text Label 14750 7700 0    50   ~ 0
VOUT
Text Label 14350 7700 2    50   ~ 0
TEST
Text Notes 15350 5150 2    197  ~ 0
LED Series Resistor\n(V_s - V_f)/I_f\nV_s = 5V, \nV_f = 2.0V, \nI_f = 25 mA \n
$EndSCHEMATC
